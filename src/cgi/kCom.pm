
# ---------------------------------------------
# filename:	kCom.pm
# author:	Frode Klevstul (frode@klevstul.com)
# started:	23.10.2002
# version:	v01_20040512
# ---------------------------------------------



# --------------------------------------------------
# package setup
# --------------------------------------------------
package kCom;
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(
	kCom_getIniValue
	kCom_printTop
	kCom_printBottom
	kCom_tableStart
	kCom_tableStop
	kCom_table
	kCom_error
	kCom_printError
	kCom_timestamp
	kCom_log
	kCom_addComment
	kCom_parse
	kCom_HTMLify
	kCom_deHTMLify
	kCom_getHostIP
	kCom_listComments
	kCom_printDate
	kCom_getLatest
	kCom_kAlbum_getLatest
	kCom_kForum_getLatest
	kCom_kIndexPlugin_logViewer
	kCom_kIndexPlugin_news
	kCom_kIndexPlugin_contactInfo
	kCom_kIndexPlugin_kSMS
	kCom_kGetMostPop
	kCom_getAgeOfFileString
	kCom_getSortableNumberString
	kCom_jumpTo
	kCom_parseBBCode
	kCom_sendEmail
	kCom_login
	kCom_notLoggedInMessage
	kCom_setCookie
	kCom_getCookie
	kCom_makeURL
	kCom_printDropdown
	kCom_verifyCode
	kCom_generateVerificationCode
);



# -----------------------------------------------
# Global declarations
# -----------------------------------------------



# -----------------------------------------------
# sub rutines
# -----------------------------------------------



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		Opens a file and returns a parameter value.
#			The file has to be on the format:
#			PARAMETER_NAME		PARAMETER_VALUE
#			(name and value has to be separated by tab)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_getIniValue{
	my $ini_file = $_[0];						# the name of the file that is going to be opened
	my $parameter = $_[1];						# the name of the parameter to what we want it's value
	my $ignore_value = $_[2];					# if this variable is equal to 1, then we ignore if no value is found
	my $stop_value = $_[3];						# this variable tells us if we just want first entry in a file or all values
												# if no value (undef), returns all entries. if value == 1, return first entrie
	my $line;
	my @value;									# NB: This procedure returns an array!

	if ($ini_file eq "kCom"){
		$ini_file	= "../kCom.ini";
	} elsif ($ini_file eq "kAlbum"){
		$ini_file	= "../kAlbum/kAlbum.ini";
	} elsif ($ini_file eq "kIndex"){
		$ini_file	= "../kIndex/kIndex.ini";
	} elsif ($ini_file eq "kIndexPlugin"){
		$ini_file	= "../kIndexPlugin/kIndexPlugin.ini";
	} elsif ($ini_file eq "kForum"){
		$ini_file	= "../kForum/kForum.ini";
	} elsif ($ini_file eq "kProject"){
		$ini_file	= "../kProject/kProject.ini";
	} elsif ($ini_file eq "kReg"){
		$ini_file	= "../kReg/kReg.ini";
	} elsif ($ini_file eq "kMovie"){
		$ini_file	= "../kMovie/kMovie.ini";
	} elsif ($ini_file eq "kNews"){
		$ini_file	= "../kNews/kNews.ini";
	} elsif ($ini_file eq "kLogin"){
		$ini_file	= "../kLogin/kLogin.ini";
	} elsif ($ini_file eq "kSendEmail"){
		$ini_file	= "../kSendEmail/kSendEmail.ini";
	} elsif ($ini_file eq "kBlogger"){
		$ini_file	= "../kBlogger/kBlogger.ini";
	}

    open(FILE_INI, "<$ini_file") || &kCom_error("kCom_getIniValue","failed to open '$ini_file'");
	LINE: while ($line = <FILE_INI>){
		if ($line =~ m/^#/){
			next LINE;
		} elsif ($line =~ m/^$parameter(\t)+(.*)$/){
			push(@value,$2);
			if ($stop_value == 1){
				last LINE;
			}
		}
	}
	close (FILE_INI);

	if ($value[0] eq "" and $ignore_value < 1){
		&kCom_error("kCom_getIniValue","failed to find a value for parameter '$parameter' in '$ini_file'");
	}

	if ($value[0] eq "NULL"){
		$value[0] = "";
	}

	return @value;
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		Writes out an error and dies
# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_error{
	my $module		= $_[0];
	my $error_msg	= $_[1];
	die "Error in module '$module': $error_msg\n";
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		Prints out an error in HTML
# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_printError{
	my $module			= $_[0];
	my $error_msg		= $_[1];
	my @errorfontcolor 	= &kCom_getIniValue("kCom","HTML_ERRORFONTCOLOR");

	print qq(<font color="$errorfontcolor[0]">Error in module '$module': $error_msg</font><br><br>\n);
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		Prints the start of every page
# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_printTop{
	my $active_module	= $_[0];
	my $base 			= $_[1];
	my @title 			= &kCom_getIniValue("kCom","HTML_PAGETITLE");
	my @bgcolor 		= &kCom_getIniValue("kCom","HTML_BODYBACKGROUNDCOLOR");
	my @linkcolor 		= &kCom_getIniValue("kCom","HTML_LINKCOLOR");
	my @alinkcolor 		= &kCom_getIniValue("kCom","HTML_ACTIVELINKCOLOR");
	my @vlinkcolor 		= &kCom_getIniValue("kCom","HTML_VISITEDLINKCOLOR");
	my @logo 			= &kCom_getIniValue("kCom","GIF_LOGO");
	my @tipsomeone 		= &kCom_getIniValue("kCom","GIF_TIPSOMEONE");
	my @line 			= &kCom_getIniValue("kCom","GIF_LINE");
	my @css				= &kCom_getIniValue("kCom","HTML_CASCADINGSTYLESHEET");
	my @url				= &kCom_getIniValue("kCom","URL_KCOM");
	my @img				= &kCom_getIniValue("kCom","URL_IMG");
	my @textcolor		= &kCom_getIniValue("kCom","HTML_TEXTCOLOR");
	my @modules 		= &kCom_getIniValue("kCom","MODULE");
	my $module;
	my @tmp;
	my $gif;
	my $name;
	my $target;
	my $active_main_module;
	my $main_module;
	my $link_url;
	my $link_url2;

	my $path;
	my $script = "$ENV{'SCRIPT_NAME'}?$ENV{'QUERY_STRING'}";

	# sets the path
	# - -
	# 1: kAlbum
	# - -
	if ($script =~ m/kAlbum\.cgi\?$/){
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; kAlbum";
	} elsif ($script =~ m/kAlbum\.cgi\?album\:(.*)/){
		my $album = $1;
		$album =~ s/^\d+\_//g;
		$album =~ s/\_/ /g;
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; <a href=\"kAlbum.cgi\">kAlbum</a> &raquo; $album";
	} elsif ($script =~ m/kAlbum\.cgi\?pic\:(.*)/){
		my $pic = $1;
		my $album = $pic;
		$album =~ s/-.*$//g;
		my $album2 = $album;
		$album =~ s/^\d+\_//g;
		$album =~ s/\_/ /g;
		$pic =~ s/^.*-//;
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; <a href=\"kAlbum.cgi\">kAlbum</a> &raquo; <a href=\"kAlbum.cgi?album:$album2\">$album</a> &raquo; $pic";
	} elsif ($script =~ m/kAlbum\.cgi\?com\:(.*)/){
		my $pic = $1;						# pic is the picture name formatted for print
		my $album = $pic;					# album is the album name formatted for print
		$album =~ s/-.*$//g;
		my $album2 = $album;				# album2 is the original album name
		$album =~ s/^\d+\_//g;
		$album =~ s/\_/ /g;
		$pic =~ s/^.*-//;
		my $string = $ENV{'QUERY_STRING'};	# string is the query string without 'com:'
		$string =~ s/com\://;
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; <a href=\"kAlbum.cgi\">kAlbum</a> &raquo; <a href=\"kAlbum.cgi?album:$album2\">$album</a> &raquo; <a href=\"kAlbum.cgi?pic:$string\">$pic</a> &raquo; add comment";
	# - -
	# 2: kForum
	# - -
	} elsif ($script =~ m/kForum\.cgi\??$/){
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; kForum";
	} elsif ($script =~ m/kForum\.cgi\?show:(.*)$/){
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; <a href=\"kForum.cgi\">kForum</a> &raquo; $1";
	} elsif ($script =~ m/kForum\.cgi\?new:(\d.*)$/){
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; <a href=\"kForum.cgi\">kForum</a> &raquo; <a href=\"kForum.cgi?show:$1\">$1</a> &raquo; Reply to Topic";
	} elsif ($script =~ m/kForum\.cgi\?new:$/){
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; <a href=\"kForum.cgi\">kForum</a> &raquo; New Topic";
	# - -
	# 3: kProject
	# - -
	} elsif ($script =~ m/kProject\.cgi\??$/ || $script =~ m/kProject\.cgi\?type=/){
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; kProject";
	} elsif ($script =~ m/kProject\.cgi\?show:(.*)$/){
		my $project = $1;
		if ($project =~ m/\/(.*)\.\w+$/) {
			$project = $1;
		}
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; <a href=\"kProject.cgi\">kProject</a> &raquo; $project";
	# - -
	# 4: kReg
	# - -
	} elsif ($script =~ m/kReg\.cgi\??$/){
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; kReg";
	} elsif ($script =~ m/kReg\.cgi\?show:(.*)$/){
		my $registration = $1;
		if ($registration =~ m/\/(.*)\.\w+$/) {
			$registration = $1;
		}
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; <a href=\"kReg.cgi\">kReg</a> &raquo; $registration";
	# - -
	# 5: kMovie
	# - -
	} elsif ($script =~ m/kMovie\.cgi\??$/){
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; kMovie";
	} elsif ($script =~ m/kMovie\.cgi\?show:(.*)$/){
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; <a href=\"kMovie.cgi\">kMovie</a> &raquo; $1";
	} elsif ($script =~ m/kMovie\.cgi\?com:(.*)$/){
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; <a href=\"kMovie.cgi\">kMovie</a> &raquo; <a href=\"kMovie.cgi?show:$1\">$1</a> &raquo; add comment";
	# - -
	# 6: kSendEmail
	# - -
	} elsif ($script =~ m/kSendEmail\.cgi\??/){
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; kSendEmail";
	# - -
	# 7: kBlogger
	# - -
	} elsif ($script =~ m/kBlogger\.cgi\??$/){
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; kBlogger";
	} elsif ($script =~ m/kBlogger\.cgi\?show:(.*)$/){
		my $post_id = $1;
		$post_id =~ s/.*\/(.*$)/$1/;
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; <a href=\"kBlogger.cgi\">kBlogger</a> &raquo; $post_id";
	} elsif ($script =~ m/kBlogger\.cgi\?com:(.*)$/){
		my $post = $1;
		my $post_id = $post;
		$post_id =~ s/.*\/(.*$)/$1/;
		$path .= "&raquo; <a href=\"$url[0]\">$title[0]</a> &raquo; <a href=\"kBlogger.cgi\">kBlogger</a> &raquo; <a href=\"kBlogger.cgi?show:$post\">$post_id</a> &raquo; add comment";
	# - -
	# x: frontpage
	# - -
	} else {
		$path = "&raquo; $title[0]";
	}

	# set base on HTML page
	if ($base ne ""){
		$base = "<base href=\"$base\">";
	}

	# set link url (for tip-someone link)
	$link_url = "http://" . $ENV{'SERVER_NAME'} . $ENV{'SCRIPT_NAME'} ."?" . $ENV{'QUERY_STRING'};

	# link_url2 for use when login
	$link_url2 = $link_url;

	# URL encode
	$link_url = &kCom_makeURL($link_url);


	print "Content-type: text/html\n\n";

	print qq(
		<html>
		<head>
		<title>$title[0]</title>
		<link rel="shortcut icon" href= "http://klevstul.com/favicon.ico">
		</head>

		$base

		<link href="$css[0]" rel="stylesheet" type="text/css">

		<body bgcolor="#$bgcolor[0]" background="../img/bg.jpg" link="#$linkcolor[0]" vlink="#$vlinkcolor[0]" alink="#$alinkcolor[0]" text="#$textcolor[0]">
		<center>
		<table width="800" border="0">
		<tr>
			<td valign="bottom" height="45">
				<a href="$url[0]" target="_top"><img title="home" alt="home" src="$img[0]/$logo[0]" border="0"></a>
			</td>
			<td valign="bottom" align="right">
				&nbsp;<a href="kSendEmail.cgi?type=link&url=$link_url"><img src="$img[0]/$tipsomeone[0]" border="0" title="Email someone a link to this page"></a>
			</td>
		</tr>
		<tr><td colspan="2"><img src="$img[0]/$line[0]" width="100%" height="2"></td></tr>
		<tr><td colspan="2">
			<table border="0" width="100%"><tr><td width=\"25%\">&nbsp;</td>
	);

	if (&kCom_login){
		foreach $module (@modules){

			if ($module =~ m/^(\t?)(\w+)(\t+)(\w+\.\w+)(\t+)(\w+)(\t+)(.*)$/){

				$module	= $2;
				$gif	= $4;
				$name	= $6;
				$target	= $8;

				# gets the main module for the active module
				if ($active_module =~ m/(mod\d+)\_\d+/){
					$active_main_module = $1;
				} else {
					$active_main_module = $active_module;
				}
				# if the module read from the ini file is a sub-module...
				if ($module =~ m/(mod\d+)\_\d+/){
					$main_module = $1;

					# if it's a sub-module of the active module
					if ($active_main_module eq $main_module){

						# '_' in front of gif to get the active-gif-name
						if ($active_module eq $module){
							print "<td><a href=\"$target\"><img border=\"0\" src=\"$img[0]/_$gif\" title=\"$name\"></a></td>\n";
						} else {
							print "<td><a href=\"$target\"><img border=\"0\" src=\"$img[0]/$gif\" title=\"$name\"></a></td>\n";
						}
					}
				} else { # the line read is not a sub module
					# '_' in front of gif to get the active-gif-name
					if ($active_module eq $module){
						print "<td><a href=\"$target\"><img border=\"0\" src=\"$img[0]/_$gif\" title=\"$name\"></a></td>\n";
					} else {
						print "<td><a href=\"$target\"><img border=\"0\" src=\"$img[0]/$gif\" title=\"$name\"></a></td>\n";
					}
				}
			}
		}
		print qq(<td align="right" valign="bottom"><a href=kLogin.cgi?action=logout class="noLine" style="font-size: 9px" >[logout]</a></td>);
	} else {																						# user is not logged in
		print qq(
			<td align="right"><form action="kLogin.cgi" method="post" name="kLogin"><input class="small" type="password" size="5" maxlength="10" name="password"><input type="hidden" name="url" value="$link_url2"> <a href="javascript:document.kLogin.submit()">login</a> | <a href="kSendEmail.cgi?type=register">register</a></form></td>
		);
		if ($path =~ m/$title[0]$/){
			$path = "";
		} else {
			$path = "&raquo; <a href=\"kIndex.cgi\">Back to front page</a> | <a href=\"javascript:history.go(-1)\">Back one page</a>";
		}

	}

	print qq(
			</tr></table>
			</td></tr>
			<tr><td colspan="2" align="right"><img src="$img[0]/$line[0]" width="80%" height="2"></td></tr>
			<tr><td colspan="2" align="left">$path</td></tr>
			<tr><td colspan="2" align="left">&nbsp;</td></tr>
			<tr><td colspan="2">
			<!-- /kCom_printTop -->
	);

}


# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		Prints the bottom of every page
# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_printBottom{
	my @bottomgif 	= &kCom_getIniValue("kCom","GIF_BOTTOM");
	my @line 		= &kCom_getIniValue("kCom","GIF_LINE");
	my @img			= &kCom_getIniValue("kCom","URL_IMG");

	print qq(
		<!-- kCom_printBottom -->
		</td></tr>
		<tr><td colspan="2"><br><br><br><br><br><img src="$img[0]/$line[0]" width="100%" height="2"></td></tr>
		<tr>
			<td class="small" align="center" colspan="2">
				| <i>Powered by:</i> <a class="noLine" href="http://klevstul.com/kCom/cgi/kForum.cgi?show:20030329_215913.txt">kCom</a>
				| <i>translate to:</i>
					<!--<a class="noLine" href="http://tinyurl.com/9q6mw">Japanese</a> :-->
					<a class="noLine" href="http://japanese.watashi.no">Japanese</a> :
					<!--<a class="noLine" href="http://tinyurl.com/e3eo8">Korean</a> :-->
					<a class="noLine" href="http://korean.watashi.no">Korean</a> :
					<!--<a class="noLine" href="http://tinyurl.com/9cosm">Mandarin</a>-->
					<a class="noLine" href="http://mandarin.watashi.no">Mandarin</a>
					<!--<a class="noLine" href="http://tinyurl.com/bvpp3">Russian</a> : -->
					<!--<a class="noLine" href="http://tinyurl.com/79u2f">Japanese 2</a> : -->
					<!--<a class="noLine" href="http://tinyurl.com/8fltm">French</a> : -->
				| <!--<i>affiliated with:</i>
					<a class="noLine" target="_blank" href="http://drinkguiden.no">drinkguiden.no</a> :
					<a class="noLine" target="_blank" href="http://freePowder.com">freePowder.com</a> :
					<a class="noLine" target="_blank" href="http://uteliv.no">uteliv.no</a>
				|-->
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center"><img src="$img[0]/$bottomgif[0]"></td>
		</tr>
		</table>
		</center>

		<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
		</script>
		<script type="text/javascript">
			_uacct = "UA-401092-1";
			urchinTracker();
		</script>

		</body>
		</html>
	);
}



sub kCom_tableStart{
	my $title 		= $_[0];
	my $width 		= $_[1];
	my $cellspacing = $_[2];
	my $cellpadding = $_[3];
	my @table_color1 	= &kCom_getIniValue("kCom","HTML_TABLEBGCOLOR");
	my @table_color2 	= &kCom_getIniValue("kCom","HTML_TABLETDCOLOR");
	my @table_color3 	= &kCom_getIniValue("kCom","HTML_TABLEFONTCOLOR");
	my @table_color4 	= &kCom_getIniValue("kCom","HTML_TABLEINNERCOLOR");

	if ($width == undef){
		$width = "100%";
	}
	if ($cellspacing == undef){
		$cellspacing = "1";
	}
	if ($cellpadding == undef){
		$cellpadding = "0";
	}

	print qq(
		<table border="0" width="$width" bgcolor="#$table_color1[0]" cellpadding="0" cellspacing="1">
		<tr>
			<td bgcolor="#$table_color2[0]"><font color="#$table_color3[0]">&nbsp;$title</font></td>
		</tr>
		<tr>
			<td>
				<table border="0" width="100%" bgcolor="#$table_color4[0]" cellpadding="$cellpadding" cellspacing="$cellspacing">
				<tr><td valign="top">
	);
}


sub kCom_tableStop{
	print qq(
			</td></tr>
			</table>
			</td>
		</tr>
		</table>
	);
}


sub kCom_table{
	my $title 	= $_[0];
	my $width 	= $_[1];
	my $text 	= $_[2];

	&kCom_tableStart($title, $width);
	print $text;
	&kCom_tableStop;
}




sub kCom_timestamp{
	my $option = $_[0];
	my $time = time;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($time);
	my $timestamp;

	$year = $year+1900;
	$mon = $mon+1;

	# puts an zero in front of one digit.
	$sec =~ s/^(\d){1}$/0$1/;
	$min =~ s/^(\d){1}$/0$1/;
	$hour =~ s/^(\d){1}$/0$1/;
	$mday =~ s/^(\d){1}$/0$1/;
	$mon =~ s/^(\d){1}$/0$1/;

	if ($option == 1) {
		# 19991109
		$timestamp = $year . $mon . $mday;
	} else {
		# 19991109_182455
		$timestamp = $year . $mon . $mday . "_" . $hour . $min . $sec;
	}

	return $timestamp;

}


sub kCom_log{
	my $module = $_[0];
	my $logline = $_[1];
	my @log_dir = &kCom_getIniValue("kCom","PATH_LOG");
	my $timestamp = &kCom_timestamp();

	open (FILE, "<$log_dir[0]/$module\.log") || &kCom_error("kCom_log","failed to open '$log_dir[0]/$module\.log' for reading");
	flock (FILE, 1);		# share reading, don't allow writing
	my $content = join("",(<FILE>));
	close (FILE);
	flock (FILE, 8);		# unlock file

	# creates an entry in the log file
	open (FILE, ">$log_dir[0]/$module\.log") || &kCom_error("kCom_log","failed to open '$log_dir[0]/$module\.log' for writing");
	flock (FILE, 2);		# lock file for writing
	print FILE "$timestamp\t$module\t$logline\n" . $content;
	close (FILE);
	flock (FILE, 8);		# unlock file
}


sub kCom_addComment{
	my $comment_file = $_[0];
	my $picture = $_[1];
	my $return_url = $_[2];
	my $verification_code = &kCom_generateVerificationCode;

	&kCom_tableStart("Add comment");
	print qq(
		<form action="kComment.cgi" name="kComment" method="post">
		<input type="hidden" name="comment_file" value="$comment_file">
		<input type="hidden" name="return_url" value="$return_url">
		<br>
		<table border="0">
		<tr>
	);
	if ($picture ne undef){
		print qq(<td><img src="$picture"></td>);
	}
	print qq(
			<td valign="bottom">
			your name or email (will be displayed):<br><input type="text" name="email" size="40" maxlength="50"><br>
			your comment:<br><input type="text" name="comment" size="80" maxlength="200"><br><br>
			re-type the number <img src="kImgFromText.cgi?string=$verification_code&isCode=1">: <input type="text" name="vcode" size="4" maxlength="4"><br>
			</td>
		</tr>
		<tr>
			<td colspan=2 align="right">
				<a href="javascript:document.kComment.submit()">add comment</a>
			</td>
		</tr>
		</table>
		</form>

	);
	&kCom_tableStop;
}



sub kCom_listComments{
	my $file		= $_[0];
	my $style		= $_[1];
	my @fontcolor	= &kCom_getIniValue("kCom","HTML_COMMENTFONTCOLOR");
	my @bgcolor		= &kCom_getIniValue("kCom","HTML_COMMENTBGCOLOR");

	if (-e $file){
		my $line;
		my $host;
		my $email;
		my $comment;
		my $date;

		open (FILE , "<$file");

		if ($style == 0){
			print qq(<table border="0" width="100%">);
			while ($line = <FILE>){
				($date,$host,$email,$comment) = split('���',$line);
				$comment = &kCom_parseBBCode($comment);
				$date = &kCom_printDate($date);
				print qq(
					<tr>
						<td bgcolor="#$bgcolor[0]"><font color="#$fontcolor[0]">$date</td>
						<td bgcolor="#$bgcolor[0]"><font color="#$fontcolor[0]">$host</td>
						<td bgcolor="#$bgcolor[0]"><font color="#$fontcolor[0]">$email&nbsp;</td>
					</tr>
					<tr><td colspan="3">$comment</td></tr>
					<tr><td colspan="3">&nbsp;</td></tr>
				);
				$host=""; $email=""; $comment=""; $date = "";
			}
			print qq(</table>);
		# style == 1 : used for editing purposes
		}elsif ($style == 1){
			while ($line = <FILE>){
				print $line;
			}
		}

		close (FILE);
	}
}



# ------------
# parse
# ------------
sub kCom_parse {
	my $special = $_[0];
    local ($name, $value, $buffer, $pair, $hold, @pairs);

    if($ENV{'REQUEST_METHOD'} eq 'GET') {
        @pairs = split(/&/, $ENV{'QUERY_STRING'});
    } else {
        read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
        @pairs = split(/&/, $buffer);
    }
    foreach $pair (@pairs) {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
	$name =~ s/\n//g;
	$name =~ s/\r//g;
	$value =~ s/\n\n/\n/g;			#Get's two newlines at linux all the time
	if (!$special == 1){
		$value =~ s/\n/�br�/g;			#Modified to show linebreak
	}
	$value =~ s/\r//g;				#Modified to show linebreak

	# converts into HTML special characters
	$value = &kCom_HTMLify($value);

    #This section checks the value portion (user input) of
    #all name/value pairs.
	$value =~ s/(<[^>]*>)//g;		#Remove this tag to permit HTML tags
	$value =~ s/\|//g;
	$value =~ s/<!--(.|\n)*-->//g;
#	$value =~ s/\s-\w.+//g;
	$value =~ s/\0//g;
	$value =~ s/\|//g;
	$value =~ s/\\//g;
	$value =~ s/system\(.+//g;
	$value =~ s/grep//g;
	$value =~ s/\srm\s//g;
	$value =~ s/\srf\s//g;
#	$value =~ s/\.\.([\/\:]|$)//g;
	$value =~ s/< *((SCRIPT)|(APPLET)|(EMBED))[^>]+>//ig;
	$value =~ s/�br�/<br>/g;			#"linebreak" substituted to <br>

    #This section checks the value portion (from element name) of
    #all name/value pairs. This was included to prevent any nasty
    #surprises from those who would hijack you forms!
    $name =~ s/(<[^>]*>)//g;
	$name =~ s/<!--(.|\n)*-->//g;
	$name =~ s/\s-\w.+//g;
	$name =~ s/\0//g;
	$name =~ s/\|//g;
	$name =~ s/\\//g;
	$name =~ s/system\(.+//g;
	$name =~ s/grep//g;
	$name =~ s/\srm\s//g;
	$name =~ s/\srf\s//g;
	$name =~ s/\.\.([\/\:]|$)//g;
	$name =~ s/< *((SCRIPT)|(APPLET)|(EMBED))[^>]+>//ig;

	$FORM{$name} = $value;
    }

    return %FORM;
}



sub kCom_HTMLify{
	my $value = $_[0];

	# converts into HTML special characters
	$value =~ s/</&lt;/g;
	$value =~ s/>/&gt;/g;
	$value =~ s/\|/&brvbar;/g;
	$value =~ s/grep/&#103;rep/g;
	$value =~ s/system/&#115;ystem/g;
	$value =~ s/\\/&#92;/g;
	$value =~ s/rm\s/&#114;m /g;
	$value =~ s/rf\s/&#114;f /g;

	return $value;
}



sub kCom_deHTMLify{
	my $value = $_[0];

	# converts HTML special characters into ascii text
	$value =~ s/&lt;/</g;
	$value =~ s/&gt;/>/g;
	$value =~ s/&brvbar;/\|/g;
	$value =~ s/&#103;/g/g;
	$value =~ s/&#115;/s/g;
	$value =~ s/&#92;/\\/g;
	$value =~ s/&#114;/r/g;

	return $value;
}



sub kCom_getHostIP{
	my $host_ip;
	if ($ENV{'REMOTE_HOST'}){
	        $host_ip = $ENV{'REMOTE_HOST'};
	}
	else{
	        $host_ip = $ENV{'REMOTE_ADDR'};
	}
	return $host_ip;
}



sub kCom_printDate{
	my $date = $_[0];
	my $option = $_[1];
	my $return_date;

	# 19991109_182455
	if ( $date =~ m/(\d{4})(\d{2})(\d{2})\_(\d{2})(\d{2})(\d{2})/ ){
		if ($option == 1 || $option eq undef){
			$return_date = "$1/$2/$3 - $4:$5";
		} elsif ($option == 2){
			$return_date = "$2/$3 - $4:$5";
		}
	} else { return "Unrecorgnisable dateformat" }

	return $return_date;
}




sub kCom_getLatest{
	my $module 		= $_[0];
	my $filetype	= $_[1];
	my $number		= $_[2];
	my @path;
	my $dir;
	my $file;
	my $age;
	my @filemoddate;
	my @return_files;
	my $counter = 0;

	if ($module eq "kAlbum"){
		@path = &kCom_getIniValue("kAlbum","PATH_ALBUM");
	} elsif ($module eq "kForum"){
		@path = &kCom_getIniValue("kForum","PATH_FORUM");
		# have to remove the last dir, because this function searches for files in sub directories
		# of the given directory
		# ex: function serches for files in all dirs under f.ex "../kForum/",
		# that means that "../kForum/threads" is searched
		$path[0] =~ s/\/\w+$//;
	}

	# gets all the directories
	opendir (D, "$path[0]");
	my @directories = grep/\w/, readdir(D);
	closedir(D);

	foreach $dir (@directories){
		# check the date for all files in the directory
		opendir (D, "$path[0]/$dir");
		my @files = grep/$filetype$/, readdir(D);
		closedir(D);

		foreach $file (@files){
			push(@filemoddate, kCom_getAgeOfFileString("$path[0]/$dir/$file"));
		}
	}

	# makes a sorted array out of files
	foreach (sort @filemoddate) {

		if ($counter < $number){
			$_ =~ s/^.*���//;
			push (@return_files, $_);
			$counter++;
	    }
	}

	return @return_files;
}






sub kCom_kAlbum_getLatest{

	my $type 	= $_[0]; # 1: pictures 2: comments
	my $number 	= $_[1];
	my @picture_dir = &kCom_getIniValue("kAlbum","PATH_ALBUM");
	my @thumb_width = &kCom_getIniValue("kAlbum","MINI_THUMB_WIDTH");
	my @latest;
	my $album;
	my $pic;
	my $org_pic;
	my @pic_desc;
	# calculates the width of the table
	my $width = $number * $thumb_width[0] + $number * 5;
	my $filetype;

	if ($type == 1){
		&kCom_tableStart("The $number newest pictures",$width);
		$filetype = "_thumb.jpg";
	} elsif ($type == 2){
		&kCom_tableStart("The $number latest commented pictures",$width);
		$filetype = ".com";
	}

	@latest = &kCom_getLatest("kAlbum",$filetype,$number);

	print "<center>\n";
	foreach (@latest){
		($album,$pic) = split("-",$_);
		$org_pic = $_;
		$org_pic =~ s/$filetype$//;
		@pic_desc = &kCom_getIniValue("$picture_dir[0]/$album/$org_pic\.txt","DESCRIPTION","1");
		$pic_desc[0] =~ s/"/'/g;
		print qq(
			<a href="kAlbum.cgi?pic:$org_pic\.jpg"><img src="$picture_dir[0]/$album/$org_pic\_thumb\.jpg" width="$thumb_width[0]" border="0" title="$pic_desc[0]"></a>
		);
	}
	print "</center>\n";
	&kCom_tableStop;

}




sub kCom_kForum_getLatest{

	my $number 	= $_[0];
	my @latest;
	my @forum_dir = &kCom_getIniValue("kForum","PATH_FORUM");
	my @thread_title;
	my $line;
	my $no_answers;
	my $thread_title;

	&kCom_tableStart("The $number latest topics in the Forum");
	@latest = &kCom_getLatest("kForum",".txt",$number);
	print "<table>\n";
	foreach (@latest){
		$no_answers = -1;
		@thread_title = &kCom_getIniValue($forum_dir[0]."/".$_,"TITLE",0,1);

	    open(FILE, "<$forum_dir[0]/$_") || &kCom_error("kCom_kForum_getLatest","failed to open '$forum_dir[0]/$_'");
		while ($line = <FILE>){
			if ($line =~ m/^HOST_IP/){
				$no_answers++;
			}
		}
		close(FILE);

		$thread_title = &kCom_deHTMLify($thread_title[0]);
		if (substr($thread_title,29,29) ne undef){
			$thread_title = substr($thread_title,0,25) . "...";
		}
		$thread_title = &kCom_HTMLify($thread_title);

		print qq(
			<tr><td><a href="kForum.cgi?show:$_">$thread_title</a> ($no_answers)</td></tr>
		);
	}
	print "</table>\n";
	&kCom_tableStop;

}





sub kCom_kIndexPlugin_logViewer{

	my $number		= $_[0];
	my $line;
	my @log_dir 	= &kCom_getIniValue("kCom","PATH_LOG");
	my $counter		= 1;

	my $timestamp;
	my $no_pics;
	my $org_album;
	my $album;

	&kCom_tableStart("Log entries","10%");
	print "<table cellpadding=\"0\" cellspacing=\"0\">\n";

    open(FILE, "<$log_dir[0]/kUpload.log");
	LINE: while ($line = <FILE>){

		if ($line =~ m/^(\d{8}\_\d{6})\tkUpload\tnewPics:(\d+):(.*)$/){
			($timestamp,$no_pics,$album) = ($1,$2,$3);
			$timestamp = &kCom_printDate($timestamp);
			$org_album = $album;
			$album =~ s/^\d+\_//g;
			$album =~ s/\_/ /g;
			print "<tr><td><nobr>$timestamp: $no_pics new pictures added in '<a href=\"kAlbum.cgi?album:$org_album\">$album</a>'</nobr></td></tr>\n";
			if ($counter < $number){
				$counter++;
			} else {
				last LINE;
			}
		} elsif ($line =~ m/^(\d{8}\_\d{6})\tkUpload\tnewAlbum:(.*)$/){
			($timestamp,$album) = ($1,$2);
			$timestamp = &kCom_printDate($timestamp);
			$org_album = $album;
			$album =~ s/^\d+\_//g;
			$album =~ s/\_/ /g;
			print "<tr><td><nobr>$timestamp: New album named '<a href=\"kAlbum.cgi?album:$org_album\">$album</a>' created</nobr></td></tr>\n";
			if ($counter < $number){
				$counter++;
			} else {
				last LINE;
			}
		}
	}
	close (FILE);

	print "</table>\n";
	&kCom_tableStop;

}





sub kCom_kIndexPlugin_news{

	my $number		= $_[0];
	my $line;
	my @newsfile	= &kCom_getIniValue("kNews","PATH_NEWSFILE");
	my $counter		= 1;

	my $timestamp;
	my $host_ip;
	my $message;

	&kCom_tableStart("News","100%");
	print "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";

    open(FILE, "<$newsfile[0]");
	LINE: while ($line = <FILE>){

		if ($line =~ m/^(\d{8}\_\d{6})���(.*)���(.*)$/){
			($timestamp,$host_ip,$message) = ($1,$2,$3);
			$timestamp = &kCom_printDate($timestamp, 2);
			print "<tr><td><nobr>$timestamp: '$message'</nobr></td></tr>\n";
			if ($counter < $number){
				$counter++;
			} else {
				last LINE;
			}
		}
	}
	close (FILE);

	print "</table>\n";
	&kCom_tableStop;

}




sub kCom_kIndexPlugin_contactInfo{

	my @address 	= &kCom_getIniValue("kIndexPlugin","CONTACT_INFO");

	&kCom_tableStart("Contact Info","");

	foreach (@address){
		print "$_\n";
	}

	&kCom_tableStop;

}


sub kCom_kIndexPlugin_kSMS{
	my @txtlength	= &kCom_getIniValue("kIndexPlugin","KSMS_TXTLENGTH");

	&kCom_tableStart("Send free SMS","95%");
	print qq(
		<form method="POST" name="freeSMS" action="kSMS.cgi">
		<table border="0" callpadding="0" cellspacing="0">
		<tr>
			<td>ph number:</td>
			<td><input type="text" name="phonenumber" size="15"> (full no, inc '+')
		</tr>
		<tr>
			<td>message:</td>
			<td><input type="text" name="message" size="30" maxlength="$txtlength[0]"></td>
		</tr>
		<tr>
			<td colspan="2" align="right"><a href="javascript:document.freeSMS.submit()">send sms</a></td>
		</tr>
		</table>
	);
	&kCom_tableStop;

}


sub kCom_kGetMostPop{

	my $type		= $_[0];
	my $number		= $_[1];
	my $line;
	my @access_log 	= &kCom_getIniValue("kCom","PATH_APACHE_ACCESS_LOG");
	my $counter		= 0;
	my $title;
	my $entry;
	my %results;
	my $max_hits = 0;
	my @res;
	my $i;

	my @picture_dir = &kCom_getIniValue("kAlbum","PATH_ALBUM");
	my @thumb_width = &kCom_getIniValue("kAlbum","MINI_THUMB_WIDTH");
	my $width = $thumb_width[0] + 5;
	my @pic_desc;
	my $album;
	my $pic;

	my $movie;

	# 127.0.0.1 - - [10/Mar/2003:11:09:30 +1000] "GET /cgi/kAlbum.cgi?pic:20030220_Australias_Answer_To-103_0328_r1.jpg HTTP/1.1" 200 3297
	# 85.164.241.227 - - [18/Dec/2005:15:00:59 +0100] "GET /kCom/cgi/kMovie.cgi?show:frodeKlevstul_showreel HTTP/1.1" 200 3795
	# 84.48.134.192 - - [23/Jul/2006:21:30:11 +0200] "GET /kCom/cgi/kForum.cgi?show:20030329_215913.txt HTTP/1.1" 200 13826 "http://klevstul.com/kCom/cgi/kForum.cgi" "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.4) Gecko/20060508 Firefox/1.5.0.4"


	if ($type =~ m/pic/){
		$title = "Pop pics";
	} elsif ($type =~ m/album/){
		$title = "Pop albums";
	} elsif ($type =~ m/movie/){
		$title = "Pop movies";
	}

    open(FILE, "<$access_log[0]");
	LINE: while ($line = <FILE>){
		$line =~ s/(.*)HTTP\/1\..*/$1/g;															# remove everything behind HTTP, otherwise it'll be wrong when refering site is logged in same logfile

		if ($type eq "pic" | $type eq "album"){

			if ($line =~ m/GET.*pic:(\S*)/){
				$entry = $1;
			}

			if ($entry ne ""){

				if ($type eq "pic"){
					$results{$entry}++;
				} elsif($type eq "album"){
					$album = $entry;
					$album =~ s/-.*$//;
					$results{$album}++;
				}
				$entry = "";
			}

		} elsif ($type eq "movie"){

			if ($line =~ m/GET.*kMovie.*show:(\S*)\s.*/){
				$entry = $1;
				$entry =~ s/"//;																	# sometimes we get an '"' in the end of the name
			}

			if ($entry ne ""){
				$results{$entry}++;
				$entry = "";
			}
		}
	}
	close (FILE);


	# makes a sorted array out of entries
	foreach (keys %results){
		push(@res, kCom_getSortableNumberString($results{$_}) . "���" . $_);
	}

	&kCom_tableStart($title,$width);
	print "<table cellpadding=\"1\" cellspacing=\"0\">\n";

	if ($type eq "pic"){

		FOREACH: foreach (reverse sort @res){
			if ($i < $number){
				($counter,$entry) = split("���",$_);
				if ($counter =~ m/^0*(.*)/ ){
					$counter = $1;
				}
				$album = $entry;
				$album =~ s/-.*$//;
				$pic = $entry;
				$pic =~ s/\.jpg//;
				if ( -e "$picture_dir[0]/$album/$pic\.txt" && "$picture_dir[0]/$album/$pic\.jpg" ){
					@pic_desc = &kCom_getIniValue("$picture_dir[0]/$album/$pic\.txt","DESCRIPTION","1");
					$pic_desc[0] =~ s/"/'/g;
					print "<tr><td><a href=\"kAlbum.cgi?pic:$entry\"><img border=\"0\" width=\"$thumb_width[0]\" src=\"$picture_dir[0]/$album/$pic\_thumb.jpg\" title=\"$counter hits: $pic_desc[0]\"></a></td></tr>\n";
					$i++;
				} else {
					next FOREACH;
				}
			} else {
				last FOREACH;
			}
		}
	} elsif ($type eq "album") {

		FOREACH: foreach (reverse sort @res){
			if ($i < $number){
				($counter,$entry) = split("���",$_);
				if ($counter =~ m/^0*(.*)/ ){
					$counter = $1;
				}

				$album = $entry;
				$album =~ s/-.*$//;
				$album =~ s/_/ /;
				$album =~ s/^\d{8}/ /;
				if ( -e "$picture_dir[0]/$entry/"){
					print "<tr><td><nobr><a href=\"kAlbum.cgi?album:$entry\">$counter: '$album'</a></nobr></td></tr>\n";
					$i++;
				} else {
					next FOREACH;
				}
			} else {
				last FOREACH;
			}
		}
	} elsif ($type eq "movie") {

		FOREACH: foreach (reverse sort @res){
			if ($i < $number){
				($counter,$entry) = split("���",$_);
				if ($counter =~ m/^0*(.*)/ ){
					$counter = $1;
				}

				$movie = $entry;
				print "<tr><td><nobr><a href=\"kMovie.cgi?show:$entry\">$counter: '$entry'</a></nobr></td></tr>\n";
				$i++;
			} else {
				last FOREACH;
			}
		}
	}

	print "</table>\n";
	&kCom_tableStop;

}



sub kCom_getAgeOfFileString{
	my $file 		= $_[0];
	my $age = (-M "$file");
	my $return_string;
	my $filename	= $file;
	$filename =~ s/^.*\///;
	$return_string = kCom_getSortableNumberString($age) . "���" . $filename;
}


sub kCom_getSortableNumberString{
	my $number 		= $_[0];
	if ($number<=9){
		$return_string =  "00000" . $number;
	} elsif ($number<=99) {
		$return_string =  "0000" . $number;
	} elsif ($number<=999) {
		$return_string =  "000" . $number;
	} elsif ($number<=9999) {
		$return_string =  "00" . $number;
	} elsif ($number<=99999) {
		$return_string =  "0" . $number;
	}
	return $return_string;
}



sub kCom_jumpTo{
	my $url			= $_[0];
	my $ctype		= $_[1];
	my $wait		= $_[2];

	my @bgcolor 	= &kCom_getIniValue("kCom","HTML_BODYBACKGROUNDCOLOR");
	my @textcolor	= &kCom_getIniValue("kCom","HTML_TEXTCOLOR");

	if ($ctype == 1){
		print "Content-type: text/html\n\n";
	}

	if ($wait eq undef){
		$wait = 0;
	}

	print qq(<html><head><title> ::: kCom ::: publishing solution ::: </title>
		<META HTTP-EQUIV="REFRESH" Content="$wait;URL=$url">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		</head><body bgcolor="#$bgcolor[0]" text="#$textcolor[0]">Please wait...</body></html>
	);
}


sub kCom_parseBBCode{
	my $string = $_[0];

	my $reg_email		= '[\w|\d|\.|\-]*\@{1}[\w|\d|\.|\-]*';
	my $reg_linkname 	= '[\w|\d|\.|\-|\s|&|\@|=|!|\/|\'|`|)|(|:|\?|<|>|"]*';
	my $reg_url			= '[\w|\d|\.|\/|\?|\$|:|\-|~|\@|&|=|\+|\%|;|#|,|)|(]*';
	my $reg_color		= '[\w|#|\d]*';
	my @img				= &kCom_getIniValue("kCom","URL_IMG");

	# -------------
	# [quote][/quote]
	# -------------
	$string =~ s/\[quote="?($reg_linkname)"?\]/$1: <i>"/ig;
	$string =~ s/\[quote\]/<i>"/ig;
	$string =~ s/\[\/quote\]/"<\/i>/ig;

	# ------------
	# [b][/b]
	# ------------
	$string =~ s/\[b\]/<b>/ig;
	$string =~ s/\[\/b\]/<\/b>/ig;

	# ------------
	# [i][/i]
	# ------------
	$string =~ s/\[i\]/<i>/ig;
	$string =~ s/\[\/i\]/<\/i>/ig;

	# ------------
	# [u][/u]
	# ------------
	$string =~ s/\[u\]/<u>/ig;
	$string =~ s/\[\/u\]/<\/u>/ig;

	# ------------
	# [color][/color]
	# ------------
	$string =~ s/\[colou?r="?($reg_color)"?\]/<font color="$1">/ig;
	$string =~ s/\[\/colou?r\]/<\/font>/ig;

	# ------------
	# [size][/size]
	# ------------
	$string =~ s/\[size="?(\d+)"?\]/<font size="$1">/ig;
	$string =~ s/\[\/size\]/<\/font>/ig;

	# ------------
	# [img][/img]
	# ------------
	$string =~ s/\[img\]($reg_url)\[\/img\]/<img src="$1">/ig;
	$string =~ s/\[img="?($reg_url)"?\]($reg_linkname)\[\/img\]/<img src="$1" title="$2">/ig;

	# ------------
	# [pre][/pre]
	# ------------
	$string =~ s/\[pre\]/<pre>/ig;
	$string =~ s/\[\/pre\]/<\/pre>/ig;

	# -------------
	# [dice][/dice]
	# -------------
	$string =~ s/\[dice\]\s?(\d{1})\s?\[\/dice\]/<img src="$img[0]\/dice$1\.gif">/ig;

	# -------------
	# [email][/email]
	# -------------
	$string =~ s/\[email/\[url/ig;
	$string =~ s/\[\/email\]/\[\/url\]/ig;

	# -------------
	# [url][/url]
	# -------------
	# [url]mailto:frode@klevstul.com[/url]
	$string =~ s/\[url\](mailto:)($reg_email)\[\/url\]/<a href="$1$2">$2<\/a>/ig;
	# [url]frode@klevstul.com[/url]
	$string =~ s/\[url\]($reg_email)\[\/url\]/<a href="mailto:$1">$1<\/a>/ig;
	# [url=mailto:frode@klevstul.com]send email[/url]
	$string =~ s/\[url="?(mailto:)($reg_email)"?\]($reg_linkname)\[\/url\]/<a href="$1$2">$3<\/a>/ig;
	# [url=frode@klevstul.com]send email[/url]
	$string =~ s/\[url="?($reg_email)"?\]($reg_linkname)\[\/url\]/<a href="mailto:$1">$2<\/a>/ig;

	# [url]http://klevstul.com[/url]
	$string =~ s/\[url\](https?|ftp)(:\/\/)($reg_url)\[\/url\]/<a href="$1$2$3" target="_blank">$3<\/a>/ig;
	# [url]klevstul.com[/url]
	$string =~ s/\[url\]($reg_url)\[\/url\]/<a href="http:\/\/$1" target="_blank">$1<\/a>/ig;
	# [url=http://klevstul.com]visit this site[/url]
	$string =~ s/\[url="?(https?|ftp)($reg_url)"?\]($reg_linkname)\[\/url\]/<a href="$1$2" target="_blank">$3<\/a>/ig;
	# [url=klevstul.com]visit this site[/url]
	$string =~ s/\[url="?($reg_url)"?\]($reg_linkname)\[\/url\]/<a href="http:\/\/$1" target="_blank">$2<\/a>/ig;

	return $string;
}



sub kCom_sendEmail{
	my $from		= $_[0];
	my $to			= $_[1];
	my $subject		= $_[2];
	my $body		= $_[3];

	my @sendmail	= &kCom_getIniValue("kSendEmail","SENDMAIL_PATH");
	my @signature	= &kCom_getIniValue("kSendEmail","EMAIL_SIGNATURE");
	my @contact		= &kCom_getIniValue("kCom","EMAIL_CONTACT");

	my @title 		= &kCom_getIniValue("kCom","HTML_PAGETITLE");
	my $hostip		= &kCom_getHostIP;

	$signature[0]	=~ s/\[TITLE\]/$title[0]/;
	$signature[0]	=~ s/\[HOSTIP\]/$hostip/;
	$signature[0]	=~ s/\\n/\n/g;
	$body			=~ s/<br>/\n/g;

	if ($to eq "contact"){																			# in case it's an email sent from the contact form
		$to = $contact[0];
	}

	if ($to =~ m/^[a-zA-Z0-9]+.+\@.+\.[a-zA-Z]{2,4}$/g){
		open (MAIL, "|$sendmail[0] -t") || &kCom_error("starting sendmail '$sendmail[0]': $!");
		print MAIL "From: $from (via $title[0]) <$from>\n";
		print MAIL "Reply-to: $from\n";
		print MAIL "To: $to\n";
		print MAIL "Subject: $subject\n";
		print MAIL "Content-type: text/plain\n\n";
		print MAIL "$body\n\n";
		print MAIL "--\n\n";
		print MAIL "$signature[0]\n\n";
		close(MAIL) || &kCom_error("closing mail: $!");

		$body =~ s/\n/<br>/sg;
		&kCom_log("kSendEmail", $from." : ".$to." : ".$body);
	} else {
		$body =~ s/\n/<br>/sg;
		&kCom_log("kSendEmail", "Invalid recipient : ".$from." : ".$to." : ".$body);
	}

}



sub kCom_login{

	my $login;

	my $cookie = &kCom_getCookie("kCom");

	if ($cookie eq "loggedIn"){
		$login = 1;
	} else {
		$login = 0;
	}

	return $login;
}



sub kCom_notLoggedInMessage{

	&kCom_printError("kLogin","Please <b>LOGIN</b> to access this page!<br><br>In case you've already logged in, please <b>RELOAD</b> this page to view it!");

}



sub kCom_setCookie {
	# end a set-cookie header with the word secure and the cookie will only
	# be sent through secure connections

	my ($name, $value, $expires, $path, $domain, $secure) = @_;

	$name 			= &kCom_cookieScrub($name);
	$value 			= &kCom_cookieScrub($value);

	$expires		= $expires * 60;

	my $expire_at 	= &kCom_cookieDate($expires);
	my $namevalue 	= "$name=$value";

	my $COOKIE		= "";

	if ($expires != 0) {
		$COOKIE		= "Set-Cookie: $namevalue; expires=$expire_at; ";
	}
	else {
		$COOKIE		= "Set-Cookie: $namevalue; ";   #current session cookie if 0
	}
	if ($path ne ""){
		$COOKIE 	.= "path=$path; ";
	}
	if ($domain ne ""){
		$COOKIE 	.= "domain=$domain; ";
	}
	if ($domain ne ""){
		$COOKIE 	.= "domain=$domain; ";
	}
	if ($secure ne ""){
		$COOKIE 	.= "$secure ";
	}

	print $COOKIE . "\n";
}



sub kCom_cookieScrub {
	# don't allow = or ; as valid elements of name or data

	my($retval) = @_;

	$retval=~s/\;//;
	$retval=~s/\=//;

	return $retval;
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# this routine accepts the number of seconds to add to the server
# time to calculate the expiration string for the cookie. Cookie
# time is ALWAYS GMT!
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_cookieDate {

  my ($seconds) = @_;

  my %mn = ('Jan','01', 'Feb','02', 'Mar','03', 'Apr','04',
            'May','05', 'Jun','06', 'Jul','07', 'Aug','08',
            'Sep','09', 'Oct','10', 'Nov','11', 'Dec','12' );
  my $sydate=gmtime(time+$seconds);
  my ($day, $month, $num, $time, $year) = split(/\s+/,$sydate);
  my    $zl=length($num);
  if ($zl == 1) {
    $num = "0$num";
  }

  my $retdate="$day $num-$month-$year $time GMT";

  return $retdate;
}



sub kCom_getCookie {

	my $name = $_[0];
	my $temp=$ENV{'HTTP_COOKIE'};
	my $key;
	my $value;

	# cookies are seperated by a semicolon and a space, this will split
	# them and return a hash of cookies
	@pairs=split(/\; /,$temp);

	foreach my $sets (@pairs) {
		($key,$value)=split(/=/,$sets);
		$clist{$key} = $value;
	}

  	my $retval=$clist{$name};

	return $retval;

}



sub kCom_makeURL {
	my $url = $_[0];

	$url =~ s/\=/%3D/g;
	$url =~ s/\&/%26/g;
	$url =~ s/\?/%3F/g;

	return $url;
}



sub kCom_printDropdown {
	my ($name, $submitScript, $selected, @list) = @_;

	my $display;
	my $value;

	print qq(
		<form action="$submitScript">
		<select name="$name" onChange="this.form.submit()">
	);

	foreach (@list){
		($display,$value) = split('\t+',$_);
		if ($value eq $selected){
			print qq(
				<option value="$value" selected>$display</option>
			);
		} else {
			print qq(
				<option value="$value">$display</option>
			);
		}
	}

	print qq(
		</select>
		</form>
	);

	return $url;
}



sub kCom_verifyCode {
	my $code		= $_[0];
	my @verify_dir	= &kCom_getIniValue("kCom","PATH_VERIFY");
	my $file		= "$verify_dir[0]/$code.ver";
	my $time		= time();
	my $f;
	my $return_value;

	# return true if file exists on OS
	if (-e $file){
		unlink $file;
		$return_value = 1;
	} else {
		$return_value = 0;
	}

	# cleanup old files (http://www.infocopter.com/perl/cleanup.html)
	opendir(D, $verify_dir[0]) || &kCom_error("kCom_log","failed to open '$verify_dir[0]");
	while ($f = readdir(D)) {
		next if $f =~ /^\./;
		next if $f !~ /^\d*\.ver/;																# has to be on the form "[digit].ver"

		my ($atime, $mtime, $ctime) = (stat($verify_dir[0]."/".$f))[8..10];
		my $age_hours = ($time - $mtime) / 3600;
		my $age_days  = int($age_hours / 24);

		# 0.1 equals six minutes, 1.0 = 1 hour (of course)
		next unless $age_hours > 1;

		unlink $verify_dir[0]."/".$f;
	}
	closedir(D);

	return $return_value;
}



sub kCom_generateVerificationCode
{
	my @verify_dir		= &kCom_getIniValue("kCom","PATH_VERIFY");
	my $random_number	= int(rand(9999));
	my $timestamp		= &kCom_timestamp();

	# create an file in the verification directory having the same name as the generated number
	open (FILE, ">$verify_dir[0]/$random_number.ver") || &kCom_error("kCom_log","failed to open '$verify_dir[0]/$random_number.ver' for writing");
	flock (FILE, 2);		# lock file for writing
	print FILE "$timestamp\n";
	close (FILE);
	flock (FILE, 8);		# unlock file

	# always change this number for security, it will be fixed again when printed out as gif
	$random_number = $random_number * 180876;

	return $random_number;
}
