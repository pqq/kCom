#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/local/bin/perl

# ---------------------------------------------
# filename:	kForum.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	29.03.2003
# version:	v01_20041119
# ---------------------------------------------

# ----------------------
# thread status values:
# ----------------------
# 0: don't show thread
# 1: show as normal
# 2: show, closed (not allowed to add more entries under topic)
# 3: stick (always on top)
# 4: stick, closed (not allow more entries)
# ---------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;


# ------------------
# declarations
# ------------------
my @forum_dir		= &kCom_getIniValue("kForum","PATH_FORUM");
my $query_string	= "$ENV{QUERY_STRING}";


# ------------------
# main
# ------------------
&kCom_printTop("kForum");

if ($query_string eq "" || $query_string eq "viewAll"){
	&kForum_forumOverview;
} elsif ($query_string =~ m/^show:(.*)/) {
	&kForum_openThread($1);
} elsif ($query_string =~ m/^new:$/) {
	&kForum_newThread();
} elsif ($query_string =~ m/^new:(.*)/) {
	&kForum_newThread($1);
} elsif ($query_string =~ m/^submit:/) {
	&kForum_addThread;
} elsif ($query_string =~ m/^update:/) {
	&kForum_update;
}
&kCom_printBottom;

# ------------------
# sub
# ------------------


sub kForum_forumOverview{
	my @no_threads					= &kCom_getIniValue("kForum","NO_THREADS");
	my @html_mostviewedcolor		= &kCom_getIniValue("kForum","HTML_MOSTVIEWEDCOLOR");
	my @html_unevencolor			= &kCom_getIniValue("kCom","HTML_UNEVENCOLOR");
	my @html_titlecolor				= &kCom_getIniValue("kCom","HTML_TITLECOLOR");
	my @access_log					= &kCom_getIniValue("kCom","PATH_APACHE_ACCESS_LOG");
	my @files;
	my @ageSortedFiles;
	my @thread_title;
	my @status;
	my %viewed_times;
	my $most_viewed					= 0;
	my $counter 					= 0;
	my $no_answers;
	my $last_entrie;
	my $line;
	my $status_txt;
	my $access_log_line;
	my $file;
	my $linecolour;

	# gets all files/threads in the forum
	opendir (D, "$forum_dir[0]");
	@files = grep /\w/, readdir(D);
	closedir(D);


	# statistics: find the number of times the thread has been shown
	# 192.168.0.3 - - [15/Apr/2004:13:08:58 +1000] "GET /private/projects/kcom/cgi/kForum.cgi?show:20040415_053431.txt HTTP/1.1" 200 3267
    open(ACCESSLOG, "<$access_log[0]");														# open access log to do retreive statistics
	while ($access_log_line = <ACCESSLOG>){
		$access_log_line =~ s/(.*)HTTP\/1\..*/$1/g;											# remove everything behind HTTP, otherwise it'll be wrong when refering site is logged in same logfile
		if($access_log_line =~ m/kForum.cgi\?show:(\d{8}_\d{6}\.txt)/sgi){					# we've got a view of a thread in the access log file
				$viewed_times{$1}++;
				if ( $viewed_times{$1}>$most_viewed ){										# save the maximum number a thread is viewed
					$most_viewed = $viewed_times{$1};
				}				
		}
	}
	close (ACCESSLOG);																		# close access log

	# go through all threads to deal with different status modes
	FOREACH: foreach (@files){

		# get the status for the thread, if no status is set we use 1 (normal)
		@status = &kCom_getIniValue($forum_dir[0]."/".$_,"STATUS",1);
		if ($status[0] eq "") {
			$status[0] = 1;
		}

		# remove all threads with status == 0
		if ($status[0]==0 && $query_string ne "viewAll") {
			next FOREACH;
		# if status is 3 or 4 we show the thread on top (sticky)
		} elsif ($status[0]>=3) {
			push(@ageSortedFiles, "000000.0000000000000000000000000000000¤¤¤".$_);
		} else {
			push(@ageSortedFiles, kCom_getAgeOfFileString($forum_dir[0]."/".$_));
		}
	}

	kCom_tableStart("Forum","","-1","2");
	
	# note: new forum commented away until I fix the spam problem
	print qq(
		<table width="100%" border="0">
		<tr><td colspan="5" align="right"><a href="kForum.cgi?new:">New Topic</a></td></tr>
		<tr><td colspan="5">&nbsp;</td></tr>
		<tr><td width="45%"><font color=\"#$html_titlecolor[0]\"><b>Article:</b></font></td><td width="15%"><font color=\"#$html_titlecolor[0]\"><b>Special:</b></font></td><td width="12%"><font color=\"#$html_titlecolor[0]\"><b>No answers:</b></font></td><td width="10%"><font color=\"#$html_titlecolor[0]\"><b>Viewed:</b></font></td><td><b><font color=\"#$html_titlecolor[0]\">Last entrie:</b></font></td></tr>
	);

	if ($query_string eq "viewAll") {
		$no_threads[0] = 9999;
	}


	FOREACH: foreach (sort reverse @ageSortedFiles){
		if ($counter < $no_threads[0]){
			$no_answers = -1;																		# starts with minus one, because we don't want to count the first message as an answer
			$_ =~ s/.*¤¤¤//;
			@thread_title = &kCom_getIniValue($forum_dir[0]."/".$_,"TITLE",0,1);

			@status = &kCom_getIniValue($forum_dir[0]."/".$_,"STATUS",1);
			if ($status[0] eq "") {
				$status[0] = 1;
			}

			$status_txt = "";
			if ($status[0]>=3){
				$status_txt .= "[sticky]";
			}

			if ($status[0]==2||$status[0]==4){
				$status_txt .= "[closed]";
			}

			if ($status[0]==0){
				$status_txt .= "[hidden]";
			}

		    open(FILE, "<$forum_dir[0]/$_") || &kCom_error("kForum_forumOverview","failed to open '$forum_dir[0]/$_'");
			while ($line = <FILE>){
				if ($line =~ m/^HOST_IP/){
					$no_answers++;
				} elsif ($line =~ m/^DATE\t+(.*)$/){
					$last_entrie = $1;
				}
			}
			close (FILE);

			$last_entrie = &kCom_printDate($last_entrie);

			if ($most_viewed == $viewed_times{$_}){
				print "<tr bgcolor=\"#$html_mostviewedcolor[0]\">";
				if ($linecolour == 1){
					$linecolour = 0;
				} else {
					$linecolour = 1;
				}
			} elsif ($linecolour == 1){
				print "<tr bgcolor=\"$html_unevencolor[0]\">";
				$linecolour = 0;
			} else {
				print "<tr>";
				$linecolour = 1;
			}
			print qq(
					<td><a href="kForum.cgi?show:$_">$thread_title[0]</a></td>
					<td>&nbsp;$status_txt</td>
					<td align="right">$no_answers&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td align="right">
			);
			print $viewed_times{$_} || 0;
			print qq(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>$last_entrie</td>
				</tr>
			);
			$counter++;
		}
	}

	print qq(<tr><td colspan="5">&nbsp;</td></tr></table>);
	kCom_tableStop;
}


sub kForum_openThread{
	my $thread			= $_[0];
	my $displayinfo		= 0;
	my $edit			= 0;
	my @display_hidden 	= &kCom_getIniValue("kCom","DISPLAY_HIDDEN");
	my @display_edit 	= &kCom_getIniValue("kCom","DISPLAY_EDIT");

	if ($thread =~ m/^(.*)\?$display_hidden[0]$/){
		$thread = $1;
		$displayinfo = 1;
	} elsif ($thread =~ m/^(.*)\?$display_edit[0]$/){
		$thread = $1;
		$edit = 1;
	} elsif ($thread =~ m/^(.*)\?.*$/){
		$thread = $1;
		$displayinfo = 0;
	}

	my @host_ip 		= &kCom_getIniValue($forum_dir[0]."/".$thread,"HOST_IP",1);
	my @title			= &kCom_getIniValue($forum_dir[0]."/".$thread,"TITLE",1);
	my @date			= &kCom_getIniValue($forum_dir[0]."/".$thread,"DATE",1);
	my @name			= &kCom_getIniValue($forum_dir[0]."/".$thread,"NAME",1);
	my @email			= &kCom_getIniValue($forum_dir[0]."/".$thread,"EMAIL",1);
	my @message			= &kCom_getIniValue($forum_dir[0]."/".$thread,"MESSAGE",1);
	my @status 			= &kCom_getIniValue($forum_dir[0]."/".$thread,"STATUS",1);
	my $i;

	if ($status[0] eq "") {
		$status[0] = 1;
	}

	kCom_tableStart("$title[0]","","-1","2");
	print qq(<table width="100%" border="0">);
	
	# normal displaying of the thread
	if (!$edit){
		for ($i=0; $i<$#host_ip+1; $i++){
			$message[$i]	= &kCom_parseBBCode($message[$i]);
			$date[$i] 		= &kCom_printDate($date[$i]);
			print qq (
				<tr>
					<td><b>$title[$i]</b></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>$message[$i]</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
			);

			if ($displayinfo == 1) {
				print qq(<td>[ <a href="mailto:$email[$i]">$name[$i]</a> | $date[$i] | $host_ip[$i]]</td>);
			} else {
				print qq(<td>[ $name[$i] | $date[$i]]</td>);
			}

			print qq(
				</tr>
				<tr>
					<td><hr size="1"></td>
				</tr>
			);
		}

		# if status is 0, 2 or 4 we don't display the "reply" link

		if ( ($status[0]!=0 && $status[0]!=2 && $status[0]!=4) || $displayinfo==1  ){
			print qq(
				<tr><td colspan="3" align="right"><a href="kForum.cgi?new:$thread">Reply to Topic</a></td></tr>
			);
		} else {
			print qq(
				<tr><td colspan="3" align="right">This thread is closed</td></tr>
			);
		}
	
	# display the thread in a form, for editing purposes
	} else {

		print qq(
			<tr><td colspan="3">
		);

		print qq(
			<table width="100%" border="0">
			<form name="kForum_status" action="kForum.cgi?update:" method="POST">
			<input type="hidden" name="thread" value="$thread">
			<input type="hidden" name="message_id" value="status">
			<tr>
				<td>Status:</td>
			</tr>
			<tr>
				<td><input type="text" size="5" maxlength="1" name="status" value="$status[0]">
				<i>(0 = hide, 1 = normal, 2 = closed, 3 = stick, 4 = closed+stick)</i></td>
			</tr>
			<tr><td>&nbsp;</td></tr>

			<tr><td><a href="javascript:document.kForum_status.submit()">Update status</a></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td><hr noshade width="100%" size="5"></td></tr>
			</table>
			</form>
		);

		for ($i=0; $i<$#host_ip+1; $i++){
			$message[$i] =~ s/<br>/\n/sgi;
			print qq(
				<table width="100%" border="0">
				<form name="kForum_$i" action="kForum.cgi?update:" method="POST">
				<input type="hidden" name="thread" value="$thread">
				<input type="hidden" name="message_id" value="$i">
				<input type="hidden" name="status" value="$status[0]">

				<tr>
					<td align="center"><font size="2">Answer number $i</font></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>Delete answer: <input type="checkbox" name="delete" value="delete"></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>Title:</td>
				</tr>
				<tr>
					<td><input type="text" size="100" maxlength="50" name="title" value="$title[$i]"></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>Message:</td>
				</tr>
				<tr>
					<td><textarea rows="20" cols="120" name="message" wrap="virtual">$message[$i]</textarea></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>Name:</td>
				</tr>
				<tr>
					<td><input type="text" size="100" maxlength="50" name="name" value="$name[$i]"></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>Email:</td>
				</tr>
				<tr>
					<td><input type="text" size="100" maxlength="100" name="email" value="$email[$i]"></td>
				</tr>
				<tr><td>&nbsp;</td></tr>

				<tr><td><a href="javascript:document.kForum_$i.submit()">Update answer</a></td></tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td><hr noshade width="100%" size="5"></td></tr>
				</table>
				</form>

			);
		}

	
	}
	
	print qq(
		<tr><td colspan="3">&nbsp;</td></tr>
		</table>
	);


	kCom_tableStop;

}



sub kForum_newThread{
	my $thread	= $_[0];
	my @thread_title;
	my $verification_code = &kCom_generateVerificationCode;

	if ($thread != undef) {
		@thread_title	= &kCom_getIniValue($forum_dir[0]."/".$thread,"TITLE",0,1);
		kCom_tableStart("$thread_title[0]","","-1","2");
	} else {
		$thread = "new";
		$thread_title[0]	= "";
		kCom_tableStart("New Topic","","-1","2");	
	}

	print qq(
		<form method="POST" action="kForum.cgi?submit:" name="kForum">
		<input type="hidden" name="thread" value="$thread">
		<table width="100%" border="0">
		<tr>
			<td>Title:</td>
		</tr>
		<tr>
			<td><input type="text" size="50" maxlength="50" name="title" value="$thread_title[0]"></td>
		</tr>
		<tr>
			<td>Message:</td>
		</tr>
		<tr>
			<td><textarea rows="8" cols="80" name="message" wrap="virtual"></textarea></td>
		</tr>
		<tr>
			<td>Name:</td>
		</tr>
		<tr>
			<td><input type="text" size="50" maxlength="50" name="name"></td>
		</tr>
		<tr>
			<td>Email:</td>
		</tr>
		<tr>
			<td><input type="text" size="50" maxlength="100" name="email"> (Email address will NOT be displayed!)</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>Re-type the number <img src="kImgFromText.cgi?string=$verification_code&isCode=1">: <input type="text" name="vcode" size="4" maxlength="4"><br></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td><a href="javascript:document.kForum.submit()">Submit answer</a></td></tr>
		<tr><td>&nbsp;</td></tr>
		</table>
		</form>
	);
	kCom_tableStop;
}


sub kForum_addThread{
	my %FORM = &kCom_parse;
	my $thread = $FORM{'thread'};
	my $title = $FORM{'title'};
	my $message = $FORM{'message'};
	my $name = $FORM{'name'};
	my $email = $FORM{'email'};
	my $vcode = $FORM{'vcode'};
	my $timestamp = &kCom_timestamp;
	my $host_ip = &kCom_getHostIP;

	# if verification code is invalid we return without writing to OS
	if (&kCom_verifyCode($vcode) != 1){
		if ($thread ne ""){
			&kCom_jumpTo("kForum.cgi?show:$thread");
		} else {
			&kCom_jumpTo("kForum.cgi");
		}
		return;
	}

	$message =~ s/\n/<br>\n/g;

	# Warning:  If two try to create a new thread in the same second this will go wrong!
	if ($thread eq "new"){
		$thread = &kCom_timestamp . ".txt";
	}

	if ($name eq ""){
		$name = "Anonymous";
	}

	if ($thread =~ m/\d{8}\_\d{6}\.txt/ && $title ne "" && $message ne ""){
		open (FILE, ">>$forum_dir[0]/$thread") || &kCom_error("kForum_addThread","failed to open '$forum_dir[0]/$thread'");
		flock (FILE, 2);		# lock file for writing
		print FILE "HOST_IP\t$host_ip\n";
		print FILE "TITLE\t$title\n";
		print FILE "DATE\t$timestamp\n";
		print FILE "NAME\t$name\n";
		print FILE "EMAIL\t$email\n";
		print FILE "MESSAGE\t$message\n";
		close (FILE);
		flock (FILE, 8);		# unlock file

		&kCom_jumpTo("kForum.cgi?show:$thread");
	} else {
		&kCom_jumpTo("kForum.cgi");
	}
	
	
}


sub kForum_update{
	my %FORM = &kCom_parse;
	my $thread = $FORM{'thread'};
	my $status = $FORM{'status'};																	# the status of the message
	my $delete = $FORM{'delete'};																	# do we want to delete the message?
	my $message_id = $FORM{'message_id'};
	my $title = $FORM{'title'};
	my $message = $FORM{'message'};
	my $name = $FORM{'name'};
	my $email = $FORM{'email'};
	my $line;
	my @host_ip;
	my @date;																						# when we read the messages the data is stored in these arrays
	my @titles;
	my @messages;
	my @names;
	my @emails;
	my $counter = 0;
	
	$message =~ s/\n/<br>\n/g;																		# replace newline with <br> + newline

	# read all the messages in the thread
	open (FILE, "<$forum_dir[0]/$thread") || &kCom_error("kForum_update","failed to open '$forum_dir[0]/$thread'");
	flock (FILE, 1);																				# share reading, don't allow writing
	LINE: while ($line = <FILE>){
		if ($line =~ m/^HOST_IP\t(.*)/){
			$host_ip[$counter] = $1;
		} elsif ($line =~ m/^TITLE\t(.*)/){
			$titles[$counter] = $1;
		} elsif ($line =~ m/^DATE\t(.*)/){
			$date[$counter] = $1;
		} elsif ($line =~ m/^NAME\t(.*)/){
			$names[$counter] = $1;
		} elsif ($line =~ m/^EMAIL\t(.*)/){
			$emails[$counter] = $1;
		} elsif ($line =~ m/^MESSAGE\t(.*)/){
			$messages[$counter] = $1;
			$counter++;																				# we only increase the counter when we have a new answer, after last field MESSAGE (not for every line)
		}
	}
	flock (FILE, 8);
	close (FILE);																					# file is unlocked when closed (?)

	# substitute the old updated message, with the new one
	if ($message_id =~ m/^\d+$/ && !$delete){
		$titles[$message_id] = $title;
		$messages[$message_id] = $message;
		$names[$message_id] = $name;
		$emails[$message_id] = $email;
	} elsif ($message_id =~ m/^\d+$/ && $delete){
		$titles[$message_id] = "DELETED¤¤¤";
	}


	# write back to the thread
	open (FILE, ">$forum_dir[0]/$thread") || &kCom_error("kForum_update","failed to open '$forum_dir[0]/$thread'");
	flock (FILE, 2);																				# lock file for writing
	print FILE "STATUS\t$status\n";

	for ($counter=0; $counter<$#host_ip+1; $counter++){
		if ($titles[$counter] ne "DELETED¤¤¤"){
			print FILE "HOST_IP\t$host_ip[$counter]\n";
			print FILE "TITLE\t$titles[$counter]\n";
			print FILE "DATE\t$date[$counter]\n";
			print FILE "NAME\t$names[$counter]\n";
			print FILE "EMAIL\t$emails[$counter]\n";
			print FILE "MESSAGE\t$messages[$counter]\n";
		}
	}
	flock (FILE, 8);
	close (FILE);

	&kCom_jumpTo("kForum.cgi?show:$thread");	
}

