#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/local/bin/perl

# ---------------------------------------------
# filename:	kMovie.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	19.09.2003
# version:	v01_20041119
# ---------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;


# ------------------
# declarations
# ------------------

my @movies 			= &kCom_getIniValue("kMovie","PATH_MOVIES");
my @moviesfile		= &kCom_getIniValue("kMovie","PATH_MOVIESFILE");
my $query_string 	= $ENV{QUERY_STRING};


# ------------------
# main
# ------------------
&kCom_printTop("kMovie");
if ($query_string eq ""){
	&kMovie_movieOverview;
} elsif ($query_string =~ m/^show:(.*)/) {
	&kMovie_playMovie($1);
} elsif ($query_string =~ m/^com:(.*)/) {
	&kMovie_addComment($1);
}
&kCom_printBottom;

# ------------------
# sub
# ------------------


sub kMovie_movieOverview{
	my @no_movies	= &kCom_getIniValue("kMovie","NO_MOVIES");
	my @title_color = &kCom_getIniValue("kCom","HTML_TITLECOLOR");
	my @name 		= &kCom_getIniValue($moviesfile[0],"NAME",1);
	my @description = &kCom_getIniValue($moviesfile[0],"DESCRIPTION",1);
	my @actors		= &kCom_getIniValue($moviesfile[0],"WITH",1);
	my @location	= &kCom_getIniValue($moviesfile[0],"LOCATION",1);
	my @when		= &kCom_getIniValue($moviesfile[0],"WHEN",1);
	my @info		= &kCom_getIniValue($moviesfile[0],"INFO",1);
	my @thumb		= &kCom_getIniValue($moviesfile[0],"THUMB",1);
	my @film		= &kCom_getIniValue($moviesfile[0],"FILM",1);
	my $i;
	my $shortname;

	if (&kCom_login){
		kCom_tableStart("Movies","","-1","2");
		print qq(<table width="100%" border="0">);

		for ($i=0; $i<$#name+1; $i++){
			$shortname = &kMovie_getShortname($film[$i]);
			print qq (
				<tr>
					<td align="center" valign="middle"><a href="kMovie.cgi?show:$shortname"><img src="$thumb[$i]" border="0" title="Play '$name[$i]'"></a></td>
					<td>
						<table>
							<tr><td width="70" valign="top"><font color="#$title_color[0]">Name:</font></td><td>$name[$i]</td></tr>
							<tr><td valign="top"><font color="#$title_color[0]">Desc:</font></td><td>$description[$i]</td></tr>
							<tr><td valign="top"><font color="#$title_color[0]">Actors:</font></td><td>$actors[$i]</td></tr>
							<tr><td valign="top"><font color="#$title_color[0]">Location:</font></td><td>$location[$i]</td></tr>
							<tr><td valign="top"><font color="#$title_color[0]">When:</font></td><td>$when[$i]</td></tr>
							<tr><td valign="top"><font color="#$title_color[0]">Info:</font></td><td>$info[$i]</td></tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="2"><hr size="1"></td></tr>
			);
		}

		print qq(</table>);
		kCom_tableStop;
	} else {
		&kCom_notLoggedInMessage;
	}
}


sub kMovie_playMovie{
	my $shortname = $_[0];
	my ($theFilm, $theThumb) = split('���',&kMovie_getFilmThumbname($shortname));

	#$theFilm = ""; # DEBUGGING ONLY

	kCom_tableStart("kCom mediaCentre","","-1","2");
	print qq(
		<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript">
		function FlashRequest(){
			void(0);
		}
		</SCRIPT>

		<table width="100%" border="0">
		<tr>
			<td align="right"><a target="_blank" href="$theFilm">open in new window</a></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td align="center">
	);

	if ($theFilm =~ m/video\.google/){
		print qq(
			<object width="400" height="300">
			<embed style="width:400px; height:326px;" id="VideoPlayback" type="application/x-shockwave-flash" src="$theFilm&hl=en"> </embed>
			</object>
		);
	} else {
		print qq(
			<OBJECT id="mplayer" name="mplayer" codeBase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715" classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95" Width = "400" Height = "475" standby="Loading Microsoft Windows Media Player ...">
			<param NAME="Filename" VALUE="$theFilm">
			<param NAME="AutoStart" VALUE="true">
			<param NAME="ShowControls" VALUE="1">
			<param NAME="ShowStatusBar" VALUE="1">
			<param NAME="ShowDisplay" VALUE="1">
			<EMBED TYPE="application/x-mplayer2" PLUGINSPAGE="http://www.microsoft.com/windows/mediaplayer/download/default.asp" SRC="$theFilm" Name="mplayer" Autostart="true" ShowControls="1" ShowStatusBar="1" ShowDisplay="1" DefaultFrame="Slide" Width = "380" Height = "450" BORDER="0" DisplaySize="2" Autosize="true">&nbsp;</EMBED>
			</OBJECT>
		);
	}

	print qq(
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td align="right">[<a href="kMovie.cgi?com:$shortname">add comment</a>]</td></tr>

	);
	print qq(</table>);

	if (-e "$movies[0]/$shortname\.com"){
		&kCom_listComments("$movies[0]/$shortname\.com");
	}

	kCom_tableStop;
}


sub kMovie_addComment{
	my $shortname = $_[0];
	my ($theFilm, $theThumb) = split('���',&kMovie_getFilmThumbname($shortname));
	my $return_url = "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}?$ENV{'QUERY_STRING'}";
	$return_url =~ s/com:/show:/;

	kCom_addComment("$movies[0]/$shortname\.com","$theThumb",$return_url);
}



sub kMovie_getShortname{
	my $film_shorttname = $_[0];

	$film_shorttname =~ s/\s//sg;
	$film_shorttname =~ s/^.*\///sg;
	#$film_shorttname =~ s/\..*$//sg;

	return $film_shorttname;
}


sub kMovie_getFilmThumbname{
	my $shortname = $_[0];

	my @name 		= &kCom_getIniValue($moviesfile[0],"NAME",1);
	my @thumb		= &kCom_getIniValue($moviesfile[0],"THUMB",1);
	my @film		= &kCom_getIniValue($moviesfile[0],"FILM",1);

	my $theFilm;
	my $theThumb;
	my $i;

	FOR: for ($i=0; $i<$#name+1; $i++){
		if ( $shortname eq &kMovie_getShortname($film[$i]) ){
			$theFilm = $film[$i];
			$theThumb = $thumb[$i];
			last FOR;
		}
	}

	return $theFilm . "���" . $theThumb;
}
