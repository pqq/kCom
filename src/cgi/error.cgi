#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/local/bin/perl

# ---------------------------------------------
# filename:	error.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	11.08.2003
# version:	v01_20030811
# ---------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;



# -----------------------------
# declarations
# -----------------------------
my $query_string = "$ENV{QUERY_STRING}";
my @cgi = &kCom_getIniValue("kCom","URL_CGI");



# -----------------------------
# main program
# -----------------------------
&kCom_printTop;
&error_main;
&kCom_printBottom;



# -----------------------------
# sub rutines
# -----------------------------
sub error_main{
	#my @email = &kCom_getIniValue("kCom","EMAIL_ERRORS");
	#my $script_url = "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}?$ENV{'QUERY_STRING'}";
	my $script_url = $ENV{'HTTP_REFERER'} . "--ErrorCode" .$query_string;
	print "<center>";
	&kCom_table("Something is  \%\&\#\"\"\!\"\!\"\#\�\"\#\%\"\%\/\&\(\)\/\& wrong...","400", "
		Please <a href=\"$cgi[0]/kSendEmail.cgi?type=error&message=$script_url\">send a message</a> where you're explaining the problem.
       ");
    print "</center>";


}
