#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/local/bin/perl

# ---------------------------------------------
# filename:	kSendEmail.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	24.10.2004
# version:	v01_20041024
# ---------------------------------------------



# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;



# ------------------
# declarations
# ------------------
my %FORM		= &kCom_parse;
my $type		= $FORM{'type'};
my $to			= $FORM{'to'};
my $from		= $FORM{'from'};
my $message		= $FORM{'message'};
my $url			= $FORM{'url'};
my $vcode		= $FORM{'vcode'};
my @subject		= &kCom_getIniValue("kSendEmail","EMAIL_SUBJECT");

if ($url eq ""){
	$url = "kIndex.cgi";
}



# ------------------
# main
# ------------------
&kCom_printTop;
if ($type eq "send"){
	if ($to ne "" && $from ne "" && $message ne ""){
		&kSendEmail_sendEmail;
	} else {
		&kCom_printError("kSendEmail", "Empty fields, email not sent.");
		&kCom_jumpTo($url, 0, 5);
	}
} else {
	&kSendEmail_printForm;
}
&kCom_printBottom;



# ------------------
# sub
# ------------------

sub kSendEmail_printForm{

	my @title 				= &kCom_getIniValue("kCom","HTML_PAGETITLE");
	my @contact_info 		= &kCom_getIniValue("kSendEmail","CONTACT_INFO");
	my $to_text 			= "";
	my $extra_text			= "";
	my $verification_code	= &kCom_generateVerificationCode;

	if ($type eq "link"){																			# if we're sending a link
		$extra_text = "<br><b>Email a link:</b><br>";
		$message = "Please check out this link:\n\n$url\n\n"
	} elsif ($type eq "contact"){																	# if we're sending an email via the contact form
		$to_text = "$title[0]";
		$to = "contact";
		$extra_text = "<br><br>$contact_info[0]<br><br><br><b>Send email:</b>";
	} elsif ($type eq "register"){																	# if we want to register
		$to_text = "$title[0]";
		$to = "contact";
		$message = "--- $title[0] registration ---\n\nI want a password to $title[0] because:\n\n(Write your reason here)"
	} elsif ($type eq "error"){																	# if we want to register
		my $hostip = &kCom_getHostIP;
		my $date = &kCom_timestamp;
		$date = &kCom_printDate($date);
		$to_text = "$title[0]";
		$to = "contact";
		$extra_text = "<br><b>Send error message:</b><br>Please describe the problem you experienced:<br>(If you don't want to use your email address leave the from field like it is.)";
		$message = "--- Information about the error: ---\n\nThe last successful request and error code:\n$message\nHost/IP: $hostip\nTime: $date\n\nAdditional Information:\n(Please fill in)";
		$from = "anonymous\@klevstul.com";
	} else {
		$to_text = $to;
	}

	kCom_tableStart("Send email");
	print qq(
	<form action="kSendEmail.cgi" name="kSendEmail" method="post">
	<input type="hidden" name="type" value="send">
	<input type="hidden" name="url" value="$url">
	
	<table border="0" width="">
	);

	if ($type ne "contact"){
		print qq(
		<tr>
			<td>
				<i><a href="kSendEmail.cgi?type=contact">Click here</a> if you meant to contact frode(at)klevstul.com</i>
			</td>
		</tr>
		);
	}

	print qq(
	<tr>
		<td>
			$extra_text 
		</td>
	</tr>
	<tr>
		<td>
			The email will not be sent unless all fields are filled out!<br><br>
		</td>
	</tr>
	<tr>
		<td>
			<b>From</b> (your email address):
		</td>
	</tr>
	<tr>
		<td align="left">
			<input type="text" name="from" size="40" maxlength="50" value="$from"><br><br>
		</td>
	</tr>
	<tr>
		<td>
			<b>To</b> (recipient's email address):
		</td>
	</tr>
	<tr>
		<td align="left">
	);
	if ($to ne ""){
		print qq(<input type="hidden" name="to" value="$to">$to_text);
	} else {
		print qq(<input type="text" name="to" size="40" maxlength="50" value="$to">);
	}
	print qq(
		</td>
	</tr>
	<tr>
		<td>
			<br><b>Message:</b>
		</td>
	</tr>
	<tr>
		<td align="left">
			<textarea cols="80" rows="10" name="message">$message</textarea>
		</td>
	</tr>
	<tr>
		<td align="left">
			re-type the number <img src="kImgFromText.cgi?string=$verification_code&isCode=1">: <input type="text" name="vcode" size="4" maxlength="4"> (used for verification to avoid spam)
		</td>
	</tr>
	<tr>
		<td align="right">
			<a href="javascript:document.kSendEmail.submit()">Send email</a>
		</td>
	</tr>


	</table>
	</form>
	);
	kCom_tableStop;
}




sub kSendEmail_sendEmail{

	if (&kCom_verifyCode($vcode) == 1){
		&kCom_sendEmail($from, $to, $subject[0], $message);											# send email

		print qq(
			sending email...<br><br>
			<b>from:</b> $from<br>
			<b>to:</b> $to<br>
			<b>message:</b><br>$message<br><br>
		);
	} else {
		print qq(
			Invalid code, no email sent...<br><br>
		);
	}
	&kCom_jumpTo($url, 0, 5);																		# jump back to wherever the user came from
}
