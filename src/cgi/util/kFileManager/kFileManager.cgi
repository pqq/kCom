#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/bin/perl

# ------------------------------------------------------------------------------------
# filename: kFileManager.pl
# author:   Frode Klevstul (frode@klevstul.com) (http://klevstul.com)
# started:  10.12.2004
# ------------------------------------------------------------------------------------
#
# Revision - Newest version at the top - - - - - - - - - - - - - - - - - - - - - - - -
# version:  v02_20050501
#           - File renaming
#           - Multiple file upload (max 4)
#           - Displaying sizes in MB as well as bytes, according to summing up 
#             the total amount of space used in a directory
#           - Different layout changes
# version:  v01_20041210
#           - The first version of this program was made.
# ------------------------------------------------------------------------------------


# -------------------
# use
# -------------------
use strict;
use LWP::Simple;
use CGI;


# -------------------
# declarations
# -------------------
my $version 					= "v02_20050501";
my $path 						= &getPath;															# retrieve the path to where this program is running
my $iniFile						= $path . "kFileManager.ini";										# the file containing all configuration values
my $query						= new CGI;
my $y;
my @fileFromClient_name;
my @fileFromClient_object;
for ($y=1; $y<5; $y++){
	$fileFromClient_name[$y]	= $query->param('fileFromClient_'.$y);								# the name of the file uploaded
	$fileFromClient_object[$y]	= $query->upload('fileFromClient_'.$y);								# the file uploaded
}
my $dir							= $query->param('dir');												# the directory the user choose
my $operation					= $query->param('operation');										# what operation to complete
my $file						= $query->param('file');											# the filename choosen (for delete)
my $fileNew						= $query->param('fileNew');											# the new filename when renaming a file

if ($operation eq ""){
	$operation = "list";
}
unless ($operation eq "open"){
	print "Content-type: text/html\n\n";
}

my @html_top					= &getIniValue("HTML_TOP");
my @html_bottom					= &getIniValue("HTML_BOTTOM");
my @directory					= &getIniValue("DIRECTORY");
my $urlPath 					= "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}";					# the url to this scrip on the webserver

for ($y=1; $y<5; $y++){
	$fileFromClient_name[$y] =~ s/.*[\/\\](.*)/$1/;													# strip everything but the filename
}


# -------------------
# main program
# -------------------
unless ($operation eq "open"){
	&checkDirectory;
	&printHtml("top");
	&printMain;
	&printHtml("bottom");
} else {
	&openFile;
}





# -------------------
# sub
# -------------------

# - - - - - - - - - - - - - - - - - - - - - - - - - -
# check that the directory is valid
# - - - - - - - - - - - - - - - - - - - - - - - - - -
sub checkDirectory{
	my $validDir	= 0;
	my $thisDir;


	foreach $thisDir (@directory){
		# adding a backslash to the end of the directory in case there is none
		if ($thisDir ne "" && ($thisDir !~ m/.*[\/\\]$/) ){
			if ($^O eq "MSWin32"){
				$thisDir .= "\\";
			} else {
				$thisDir .= "/";
			}
		}
		if ($dir eq $thisDir || $dir eq ""){
			$validDir = 1;
		}
	}

	if (!$validDir){
		kError("Invalid directory: '$dir' is not specified in '$iniFile'",0);
		$dir = "";
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - -
# prints the main part of the page
# - - - - - - - - - - - - - - - - - - - - - - - - - -
sub printMain{

	my $dirText;
	if ($dir eq ""){
		$dirText = "[Please load directory]";
	}

	print qq(
		<table border="0" width="100%">
		<tr><td><hr noshade size="1"></td></tr>
		<tr><td align="center"><font size="3">kFileManager</font></td></tr>
		<tr><td align="center"><i>$version</i></td></tr>
		<tr><td><hr noshade size="1"></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td><i>Current Working Directory:</i> $dir $dirText</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>
			<form action="$urlPath" name="form_chooseDir" method="post">
			<i>Change Working Directory:</i>
			<select name="dir">
	);
	foreach (@directory){
		if ($_ eq $dir) {
			print qq(<option value="$_" selected>$_</option>\n);
		} else {
			print qq(<option value="$_">$_</option>\n);
		}
	}
	print qq(
			</select>
			<a href="javascript:document.form_chooseDir.submit()">load directory</a>
			</form>
		</td></tr>
		<tr><td><hr noshade size="1"></td></tr>
		</table>
	);


	if ($dir && $operation eq "list"){
		&listDirectory;
	} elsif ($operation eq "delete") {
		&confirmDelete;
	} elsif ($operation eq "doDelete") {
		&doDelete;
	} elsif ($operation eq "upload") {
		&upload;
	} elsif ($operation eq "doRename") {
		&doRename;
	} elsif ($operation eq "renameFile") {
		&renameFile;
	}

	print qq(
		<br>
		<hr noshade size="1">
	);

}


# - - - - - - - - - - - - - - - - - - - - - - - - - -
# list out all files in a directory
# - - - - - - - - - - - - - - - - - - - - - - - - - -
sub listDirectory{
	my @files;
	my $i	= 1;
	my $size_bytes;
	my $size_megaBytes;
	my $totalSize_bytes = 0;
	my $totalSize_megaBytes;
	my $modificationDate;

	# read all files in the directory
	opendir (DIR, "$dir");
	@files = grep /\w/, readdir(DIR);
	closedir(DIR);

	print qq(
		<br>
		<table border="0" width="100%">
	);

	print qq(
		<tr><td colspan="6"><b>Upload new file(s) to "$dir":</b></td></tr>
		);

	print qq(
		<tr>
			<td colspan="6" align="right">
				<form method="post" enctype="multipart/form-data" action="$urlPath" name="form_upload">
				<input type="hidden" name="operation" value="upload">
				<input type="hidden" name="dir" value="$dir">

				<input type="file" size="45" name="fileFromClient_1" value="">
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="file" size="45" name="fileFromClient_2" value="">
				<br>
				<input type="file" size="45" name="fileFromClient_3" value="">
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="file" size="45" name="fileFromClient_4" value="">
				<br><br>
				<a href="javascript:document.form_upload.submit()">Upload new file(s)</a>
			</td>
		</tr>
	);

	print qq(

		<tr><td colspan="6">&nbsp;</td></tr>
		<tr><td colspan="6"><b>All files in "$dir"</b>:<br><br></td></tr>
		<tr>
			<td width="20">&nbsp;</td>
			<td width="40%">&nbsp;&nbsp;<b>File:</b>&nbsp;&nbsp;</td>
			<td>&nbsp;&nbsp;<b>Size:</b>&nbsp;&nbsp;</td>
			<td>&nbsp;&nbsp;<b>Date:</b>&nbsp;&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	);

	foreach (sort @files){

		# ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks)
		# = stat($filename);

		if (!-d $dir . $_){
			$size_bytes			= (-s $dir . $_);
			$totalSize_bytes	+= $size_bytes;
			$size_megaBytes		= $size_bytes/1000000;
			$size_megaBytes		+=  0.0005;
			$size_megaBytes		=~s/(^\d{1,}\.\d{3})(.*$)/$1/;
			$modificationDate	= (stat($dir . $_))[9];
			$modificationDate	= &getPrintDate($modificationDate);

			if ($i%2==0){
				print qq(<tr bgcolor="222222">);
			} else {
				print qq(<tr>);
			}

			print qq(
					<td align="right" valign="top">$i:&nbsp;&nbsp;</td>
					<td valign="top"><a href="$urlPath?operation=open&file=$dir$_" target="_blank">$_</a>&nbsp;&nbsp;</td>
					<td valign="top">$size_megaBytes MB ($size_bytes bytes)&nbsp;&nbsp;</td>
					<td valign="top">$modificationDate&nbsp;&nbsp;</td>
					<td valign="top"><a href="$urlPath?operation=renameFile&file=$_&dir=$dir">Rename</a></td>
					<td valign="top"><a href="$urlPath?operation=delete&file=$_&dir=$dir">Delete</a></td>
				</tr>
			);
			$i++;
		}
	}

	$totalSize_megaBytes = $totalSize_bytes/1000000;
	$totalSize_megaBytes +=  0.0005;
	$totalSize_megaBytes =~s/(^\d{1,}\.\d{3})(.*$)/$1/;

	print qq(
		<tr>
			<td align="right">&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="4"><b>= $totalSize_megaBytes MB ($totalSize_bytes bytes)</b></td>
		</tr>
	);

	print "</table>";
}


# - - - - - - - - - - - - - - - - - - - - - - - - - -
# ask user for confirmation before deleting a file
# - - - - - - - - - - - - - - - - - - - - - - - - - -
sub confirmDelete{
	print qq(
		<table>
		<form action="$urlPath" name="form_confirmDelete" method="post">
		<input type="hidden" name="operation" value="doDelete">
		<input type="hidden" name="file" value="$file">
		<input type="hidden" name="dir" value="$dir">
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>Are you sure you want to delete the file '<b>$file</b>'?</td>
		</tr>
		<tr>
			<td>($dir$file)</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td><a href="javascript:document.form_confirmDelete.submit()">Delete the file</a> | <a href="javascript:history.go(-1)">Do NOT delete</a></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		</table>
	);
}


# - - - - - - - - - - - - - - - - - - - - - - - - - -
# delete / unlink the file from the operating system
# - - - - - - - - - - - - - - - - - - - - - - - - - -
sub doDelete{
	my $time = time;
	my $url = $urlPath . "?dir=$dir&tmp=$time";

	unlink("$dir$file");

	&kJumpTo($url);
}



# - - - - - - - - - - - - - - - - - - - - - - - - - -
# upload new file
# - - - - - - - - - - - - - - - - - - - - - - - - - -
sub upload{
	my $time = time;
	my $url = $urlPath . "?dir=$dir&tmp=$time";
	my $i;
	my $tmpObject;

	print "Uploading the file(s):<br>\n";
	for ($i=1; $i<5; $i++){
		if ($fileFromClient_name[$i] ne ""){
			print "$i: '$fileFromClient_name[$i]'<br>";
		}
		$tmpObject = $fileFromClient_object[$i];

		open UPLOADFILE, ">$dir$fileFromClient_name[$i]";
		binmode UPLOADFILE;
		while (<$tmpObject>){
			print UPLOADFILE;
		}
		close UPLOADFILE;
	}


	&kJumpTo($url);
}


# - - - - - - - - - - - - - - - - - - - - - - - - - -
# rename file
# - - - - - - - - - - - - - - - - - - - - - - - - - -
sub renameFile{
	my $time = time;
	my $url = $urlPath . "?dir=$dir&tmp=$time";
	my $i;
	my $tmpObject;

	print qq(
		<br><br><br>
		<form action="$urlPath" name="form_doRename" method="post">
		<input type="hidden" name="operation" value="doRename">
		<input type="hidden" name="dir" value="$dir">
		<input type="hidden" name="file" value="$file">
		<input type="text" size="80" name="fileNew" value="$file">
		<a href="javascript:document.form_doRename.submit()">Rename the file</a> | <a href="javascript:history.go(-1)">Do NOT rename</a>
		</form>
		<br><br><br>
	);

}


sub doRename{
	my $time = time;
	my $url = $urlPath . "?dir=$dir&tmp=$time";
	my $i;
	my $tmpObject;

	rename ($dir.$file, $dir.$fileNew) || kError("Could not rename the file '$file'");;

	&kJumpTo($url);
}


# - - - - - - - - - - - - - - - - - - - - - - - - - -
# print out the HTML code specified as top and bottom in the ini file
# - - - - - - - - - - - - - - - - - - - - - - - - - -
sub printHtml{
	my $type = $_[0];
	my $object;
	my $url;

	if ($type eq "top"){
		$url = $html_top[0];
	} elsif ($type eq "bottom"){
		$url = $html_bottom[0];
	}

	if ($url ne ""){
		$object = &get($url);
	}

	print $object;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - -
# open a file
# - - - - - - - - - - - - - - - - - - - - - - - - - -
sub openFile{

	my $header;
	my $filetype	= $file;
	$filetype 		=~ s/.*[\.](.*)/$1/;

	# -----------------------------------------------------------------------------
	# Overview:
	#
	# http://www.w3schools.com/media/media_mimeref.asp
	# http://www.utoronto.ca/webdocs/HTMLdocs/Book/Book-3ed/appb/mimetype.html
	# -----------------------------------------------------------------------------

	if ($filetype eq "txt"){
		$header = "Content-type: text/plain\n\n";
	} elsif ($filetype eq "html"){
		$header = "Content-type: text/html\n\n";
	} elsif ($filetype eq "css"){
		$header = "Content-type: text/css\n\n";
	} elsif ($filetype eq "jpg" || $filetype eq "jpeg" || $filetype eq "jpe"){
		$header = "Content-type: image/jpg\n\n";
	} elsif ($filetype eq "gif"){
		$header = "Content-type: image/gif\n\n";
	} elsif ($filetype eq "png"){
		$header = "Content-type: image/png\n\n";
	} elsif ($filetype eq "bmp"){
		$header = "Content-type: image/bmp\n\n";
	} elsif ($filetype eq "mov" ){
		$header = "Content-type: video/quicktime\n\n";
	} elsif ($filetype eq "mpeg" || $filetype eq "mpg" || $filetype eq "mpe"){
		$header = "Content-type: video/mpeg\n\n";
	} elsif ($filetype eq "asf"){
		$header = "Content-type: video/x-ms-asf\n\n";
	} elsif ($filetype eq "asx"){
		$header = "Content-type: video/x-ms-asx\n\n";
	} elsif ($filetype eq "wmv"){
		$header = "Content-type: video/mpeg\n\n";
	} elsif ($filetype eq "avi"){
		$header = "Content-type: video/mpeg\n\n";
	} elsif ($filetype eq "mp3"){
		$header = "Content-type: audio/mpeg\n\n";
	} elsif ($filetype eq "wav"){
		$header = "Content-type: audio/mpeg\n\n";
	} elsif ($filetype eq "zip"){
		$header = "Content-type: application/zip\n\n";
	} elsif ($filetype eq "pdf"){
		$header = "Content-type: application/pdf\n\n";
	} else {
		$header = "Content-type: text/html\n\n";
	}

	print $header;

	open FILE, "<$file";
	binmode FILE;
	while (<FILE>){
		print $_;
	}
	close FILE;

}


# - - - - - - - - - - - - - - - - - - - - - - - - - -
# find the path to where the program is running
# - - - - - - - - - - - - - - - - - - - - - - - - - -
sub getPath{
	$0=~/^(.+[\\\/])[^\\\/]+[\\\/]*$/;
	my $cgidir= $1 || "./";
	return $cgidir;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# load initial config values
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub getIniValue{
	my $parameter = $_[0];
	my $line;
	my @value;

	$parameter =~ tr/a-z/A-Z/;																		# convert the parameter to uppercase

    open(FILE, "<$iniFile") || kError("failed to open '$iniFile'");
	LINE: while ($line = <FILE>){
		if ($line =~ m/^#/){
			next LINE;
		} elsif ($line =~ m/^$parameter(\t)+(.*)$/){
			push(@value,$2);
		}
	}
	close (FILE);

	if ($value[0] eq ""){
		&kError("Failed to find the values for module '$parameter' in '$iniFile'",1);
	}

	return @value;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Prints out error message
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kError{
	my $text = $_[0];
	my $exit = $_[1];

	&kPrint("ERROR: ".$text);

	if ($exit){
		exit;
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Prints out message on screen
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kPrint{
	my $text = $_[0];
	
	print $text . "\n";
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Jump to another page
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kJumpTo{
	my $url			= $_[0];

	print qq(<html><head><title></title>
		<META HTTP-EQUIV="REFRESH" Content="0;URL=$url">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		</head><body>Please wait...</body></html>
	);
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Print date
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub getPrintDate{
	my $time = $_[0];
	if ($time eq ""){
		$time = time;
	}
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($time);
	my $timestamp;

	$year = $year+1900;
	$mon = $mon+1;

	# puts an zero in front of one digit.
	$sec =~ s/^(\d){1}$/0$1/;
	$min =~ s/^(\d){1}$/0$1/;
	$hour =~ s/^(\d){1}$/0$1/;
	$mday =~ s/^(\d){1}$/0$1/;
	$mon =~ s/^(\d){1}$/0$1/;

	# 19991109_182455
	$timestamp = $year .".". $mon .".". $mday . " " . $hour .":". $min;

	return $timestamp;
}
