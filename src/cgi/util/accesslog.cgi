#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/bin/perl

##########################################################################################
#
#	accesslog.cgi
#	�2000, Perl Services
#
#	Requirements:		UNIX Server, Perl5+
#	Created:		November 27th, 2001
#	Author: 		Jim Melanson, President and Founder
#	Contact:		www.perlservices.net
#				Phone:		1-705-437-3283
#				Fax:		1-208-694-1613
#				E-Mail:		info@perlservices.net
#
##########################################################################################
#
#       Log Format:
#       This script is set to read one of two formats (standard) for an access log
#       output:
#
#       Combined Access/Referer Log
#       i.e.: 149.225.153.97 - - [29/Oct/2001:01:52:44 -0700] "GET /images/common/banner.gif HTTP/1.1" 200 3678 "http://www.thebeaches.to/somepage.html" "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; Mozilla/4.0 (compatible; MSIE 5.0; Windows 95/NT; UUNET-DE))"
#       format:   ip.ip.ip.ip - - [dd/mm/yyyy:hh:mm:ss -offset] "METHOD /file/requested.file Compliancy" code size "http://www.somereferer.com/" "UserAgent"
#
#       Access Log
#       i.e.: 149.225.153.97 - - [29/Oct/2001:01:52:44 -0700] "GET /images/common/banner.gif HTTP/1.1" 200 3678
#       format:   ip.ip.ip.ip - - [dd/mm/yyyy:hh:mm:ss -offset] "METHOD /file/requested.file Compliancy" code size
#
#       If your access log is differently formatted form the above, we can modify the script to read
#       your log. To arrange this, contact support@perlservices.net and you will be given a list of what
#       we need. The cost for this modification is US$40.
#
##########################################################################################
#
#	Log File Path:
#	Set  this variable to the absolute path to your servers/domains error log. This is
#	the error log written to by the Perl interpretor on STDERR. You will usually have to
#	ask your support/host/SysAdmin for this path.

#my $ACC_LOG = "/home/klevstul/log/access_log";
my $ACC_LOG = "C:/klevstul/private/projects/kCom/logs/access_log";

#       If you want the access log to exclude displaying certain referers in the
#       Analyze Log function, then list the domains here in the format:
#       my $exclude_referers = qq~
#       http://somedomain.com
#       http://www.somedomain.com
#       ~;
#
#       Don't forget the "http://" !!

my $exclude_referers = qq~
http://somedomain.com
http://www.somedomain.com
~;

#
##########################################################################################
#
#					EDIT NOTHING
#				      BELOW THIS LINE
#
##########################################################################################

my $ScriptURL = "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}";
my $FONT = qq~<FONT NAME="verdana,helvetica,arial" SIZE="2" color="#000080">~;

@exclude_referers = split(/\n/, $exclude_referers);

&Parse;

$FORM{numlines} ||= 25;
$FORM{reverse} ||= 1;

#The following error codes and status messages were taken found at:
#http://www.123webmaster.com/Onsite/Reference/ErrorCodes.html
my %status_code = (
    200 => 'Ok - request has succeeded without error.',
    201 => 'Created - The POST request has been done.',
    202 => 'Accepted - The asynchronous request was received but not necessarily acted upon.',
    204 => 'No Content - Request succeeded, but there is nothing to display.',
    300 => 'Multiple Choices - The requested resource is available from multiple places.',
    301 => 'Moved Permanently - Document has been moved permanently.',
    302 => 'Moved Temporarily - Document has been moved temporarily.',
    304 => 'Not Modified - Get request worked, but it contains content not modified since the set date.',
    400 => 'Bad Request - Request not understood',
    401 => 'Unauthorized - You are not authorized to view the selected document.',
    403 => 'Forbidden - Server refused to grant your request. Authorization may not have been authenticated. The file does not have "read permissions" set for all users.',
    404 => 'Not Found - The document was not found on the server.',
    405 => 'Method Not Allowed - The specified method of request is not allowed for the identified resource.',
    406 => 'Not Acceptable - The requested resource is in an invalid format according to the accept headers.',
    407 => 'Proxy Authentication Required - You are unauthorized to view the requested document, unless you can authenticate yourself with the proxy.',
    408 => 'Request Timeout - The request that you made has taken longer then the server is set to wait.',
    409 => 'Conflict - A conflict has occurred between your request and a server resource.',
    410 => 'Gone - The resource is no longer found on the server. All links to this resource should be removed, further access should not be attempted.',
    411 => 'Length Required - You may not access this information without supplying the content length in your request header.',
    414 => 'Request-URI Too Long - The requested URI is larger then the server would process.',
    415 => 'Unsupported Media Type - The format of the request was not supported by the requested resource.',
    500 => 'Internal Server Error - An error has occurred with the server. Usually a CGI application. Your request could not be processed.',
    501 => 'Not Implemented - Server does not support your request.',
    502 => 'Bad Gateway - The proxy server received an invalid response from the contacted server.',
    503 => 'Service Unavailable - The server is temporarily unable to handle the request.',
    504 => 'Gateway Timeout - The server did not receive a request from its provider and has closed the connection.',
    505 => 'HTTP Version Not Supported - The server does not support the HTTP protocol version that was used.',
);

if($FORM{analyzelog} == 1) {
    &AnalyzeLog;
} else {
    &DisplayLog;
}


sub DisplayLog {
    &PrintAccessLogHeader;

    if(open(LOG, "<$ACC_LOG")) {
        flock(LOG, LOCK_EX);
        #First we have to total th enumber of lines
        my $line_total = 0;
        while(<LOG>) {
            $first_record = $_ unless $first_record;
            $line_total++;
        }
        print qq~<B>This log file contains a total of $line_total records.</B><BR><BR>~;
        my @first_record = split_entry($first_record);
        print qq~<B>Stats recorded since $first_record[1] - $first_record[2]</B><BR><BR>\n~;

        #Go back to the beginning of the log file
        seek LOG, 0, 0;
        my $loop = 0;
        my %LINES;
        while(<LOG>) {
            $loop++;
            if($FORM{reverse} == 1) {
                if($loop >= ($line_total - $FORM{numlines})) {
                    if($_ =~ /[a-zA-Z1-9]/) {
                        $LINES{$loop} = format_output($_);
                        $LINES{$loop} .= qq~<HR>\n~;
                    }
                }
            } else {
                if($loop <= $FORM{numlines}) {
                    if($_ =~ /[a-zA-Z1-9]/) {
                        print format_output($_);
                        print "<HR>\n"
                    } else {
                        $loop--;
                    }
                } else {
                     last;
                }
            }
        }
        if($FORM{reverse} == 1) {
            my @reverse_keys = sort { $a <=> $b } keys %LINES;
            for(my $a = $#reverse_keys; $a >= 0; $a--) {
                print $LINES{$reverse_keys[$a]};
            }
        }
    } else {
        print qq~<BR><BR><BR><FONT COLOR="#FF0000"><B>Unable to open Access Log!</B><BR><BR>Is the path "$ACC_LOG" correct?<BR><BR><BR><BR>\n~;
    }
    &PrintAccessLogFooter;
}

sub AnalyzeLog {
    &PrintAccessLogHeader;
    if(open(LOG, "<$ACC_LOG")) {
        flock(LOG, LOCK_EX);
        #First we have to total th enumber of lines
        my $line_total = 0;

        #Analyze hashes and vars. We create these hashes and vars to store data in.
        my %error_401 = ();
        my %error_403 = ();
        my %error_404 = ();
        my %error_500 = ();
        my %referer_total = ();
        my %referer_list = ();
        my $first_record;
        while(<LOG>) {
            $first_record = $_ unless $first_record;
            $line_total++;
            my $analyzeloop = 0;
            my($ip, $date, $time, $method, $document_qs, $document, $qs, $code, $size, $referer, $agent) = split_entry($_);

            #Now set the hash of important errors
            if($code == 401) {
                $error_401{$document}++;
            }
            elsif($code == 403) {
                $error_403{$document}++;
            }
            elsif($code == 404) {
                $error_404{$document}++;
            }
            elsif($code == 500) {
                $error_500{$document}++;
            }
            #Now get the most offten requested documents
            elsif($code =~ /^2/) {
                $error_200plus{$document}++;
            }

            #Now build list of referers for each document
            my $ref_list_key = $document . '::' . $referer;
            $referer_list{$ref_list_key}++;

            #Total by referer
            $referer_total{$referer}++;
        }

        my @first_record = split_entry($first_record);
        print qq~<B>Stats recorded since $first_record[1] - $first_record[2]</B><BR><BR>\n~;

        print qq~<DL><DT><B>Error 401:<B><DD>$status_code{401}<BR>~;
        foreach(sort { $error_401{$b} <=> $error_401{$a} } keys %error_401) {
            print "$error_401{$_} => $_<BR>\n";
        }

        #
        print qq~<BR><BR><DT><B>Error 403:<B><DD>$status_code{403}<BR>~;
        foreach(sort { $error_403{$b} <=> $error_403{$a} } keys %error_403) {
            print "$error_403{$_} => $_<BR>\n";
        }

        #
        print qq~<BR><BR><DT><B>Error 404:<B><DD>$status_code{404}<BR>~;
        foreach(sort { $error_404{$b} <=> $error_404{$a} } keys %error_404) {
            print "$error_404{$_} => $_<BR>\n";
        }

        #
        print qq~<BR><BR><DT><B>Error 500:<B><DD>$status_code{500}<BR>~;
        foreach(sort { $error_500{$b} <=> $error_500{$a} } keys %error_500) {
            print "$error_500{$_} => $_<BR>\n";
        }

        #
        print qq~<BR><BR><DT><B>Referer Totals:<B><DD>~;
        foreach(sort { $referer_total{$b} <=> $referer_total{$a} } keys %referer_total) {
            if(referer_compare($_)) {
                print "$referer_total{$_} => $_<BR>\n";
            }
        }

        #
        print qq~<BR><BR><DT><B>Referers by document:<B><DD><DL>~;
        my %doc_printed;
        #$referer_list{"$document::$referer"}
        foreach(sort { $a cmp $b } keys %referer_list) {
            my($doc, $ref) = split(/\:\:/, $_);
            if(referer_compare($ref)) {
                if($doc_printed{$doc}) {
                    print "$referer_list{$_} => $ref<BR>\n";
                } else {
                    print "<DT><B>$doc</B><BR><DD>\n";
                    $doc_printed{$doc} = 1;
                }
            }
        }

        #
        print qq~</DL><BR><BR><DT><B>Document Hits:<B><DD>~;
        foreach(sort { $error_200plus{$b} <=> $error_200plus{$a} } keys %error_200plus) {
            print "$error_200plus{$_} => $_<BR>\n";
        }

        print "</DL>\n";

    } else {
        print "<BR><BR><BR><FONT COLOR=\"#FF0000\"><B>Unable to open Access Log!</B><BR><BR>Is the path \"$ACC_LOG\" correct?<BR><BR><BR><BR>\n";
    }
    &PrintAccessLogFooter;
}

sub referer_compare {
    my $ref_compare = shift;
    foreach(@exclude_referers) {
        if($_ =~ /[a-zA-Z1-9]/) {
            if($ref_compare =~ /^$_/) {
                return(0);
            }
        }
    }
    return(1);
}

sub format_output {
    my $line = shift;
    #12.45.78.4 - - [26/Nov/2001:07:58:06 -0700] "POST /en/programs/page_updater/pu_admin.cgi?&pid=33567 HTTP/1.1" 200 4679 "http://www.charityware.ws/en/programs/page_updater/pu_admin.cgi" "Mozilla/4.0 (compatible; MSIE 5.12; Mac_PowerPC)"
    my($ip, $date, $time, $method, $document_qs, $document, $qs, $code, $size, $referer, $agent) = split_entry($line);
    if(($ip == 0) && !$date && !$time) {
        return("<B>Line Not Recognized</B>:<BR>$line");
    } else {
        if($code > 0) {
            my($code_font_open, $code_font_close);
            if($code =~ /^(4|5)/) {
                $code_font_open = qq~<B><FONT COLOR="#FF0000">~;
                $code_font_close = qq~</font></b>~;
            }
            $code = qq~<A HREF="javascript:alert('$status_code{$code}')">$code_font_open$code$code_font_close</A>~;
        }
        my $formated_output = qq~
<B>IP:</B>$ip &nbsp;&nbsp;&nbsp; <B>DATE:</B>$date &nbsp;&nbsp;&nbsp; <B>Time:</B>$time &nbsp;&nbsp;&nbsp; <B>STATUS:</B>$code 
&nbsp;&nbsp;&nbsp; <B>SIZE:</B> $size<BR>
<B>Document:</B> $document_qs~;
        if($referer =~ /[a-zA-Z1-9]/) {
            $formated_output .= qq~<BR><B>Referer</B> $referer~;
        }
        if($agent =~ /[a-zA-Z1-9]/) {
            $formated_output .= qq~<BR><B>Agent</B> $agent~;
        }
        return($formated_output);
    }
}





sub Parse {
    local($name, $value, $buffer, $pair, $hold, @pairs);

    if($ENV{'REQUEST_METHOD'} eq 'GET') {
        @pairs = split(/&/, $ENV{'QUERY_STRING'});
    } else {
        read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
        @pairs = split(/&/, $buffer);
    }
    foreach $pair (@pairs) {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
	$name =~ s/\n//g;
	$name =~ s/\r//g;
	$value =~ s/\n//g;
	$value =~ s/\r//g;

      #This section checks the value portion (user input) of
      #all name/value pairs.
	$value =~ s/<.+>?//g;		#Remove this tag to permit HTML tags
	$value =~ s/\|//g;
	$value =~ s/<!--(.|\n)*-->//g;
	$value =~ s/\s-\w.+//g;
	$value =~ s/system\(.+//g;
	$value =~ s/grep//g;
	$value =~ s/\srm\s//g;
	$value =~ s/\srf\s//g;
	$value =~ s/\.\.([\/\:]|$)//g;
	$value =~ s/< *((SCRIPT)|(APPLET)|(EMBED))[^>]+>//ig;

      #This section checks the value portion (from element name) of
      #all name/value pairs. This was included to prevent any nasty
      #surprises from those who would hijack you forms!
        $name =~ s/<.+>?//g;
	$name =~ s/<!--(.|\n)*-->//g;
	$name =~ s/^\s-\w.+//g;
	$name =~ s/system\(.+//g;
	$name =~ s/grep//g;
	$name =~ s/\srm\s//g;
	$name =~ s/\srf\s//g;
	$name =~ s/\.\.([\/\:]|$)//g;
	$name =~ s/< *((SCRIPT)|(APPLET)|(EMBED))[^>]+>//ig;

	$FORM{$name} = $value;
    }
}

sub PrintAccessLogHeader {
    print qq~Pragma: no-cache\nContent-type: text/html\n\n~;
    print qq~<HEAD><TITLE>Access Log Viewer</TITLE></HEAD>
<BODY BGCOLOR="#F9F9F9" TEXT="#000080" LINK="ROYALBLUE" ALINK="ROYALBLUE" VLINK="ROYALBLUE">
<TABLE BORDER=0>
  <TR>
    <TD VALIGN="TOP" ALIGN="CENTER">
      &nbsp;
    </TD>
    <TD>
      <FONT FACE="verdana,helvetica,arial" SIZE=7><B>Access Log Viewer</B></FONT>
      <BR>$FONT Provided by <A HREF="http://www.perlservices.net/">Perl Services</A></FONT>
    </TD>
  </TR>
</TABLE><BR>
<TABLE BORDER=2 WIDTH=100% CELLPADDING=5>
  <TR BGCOLOR="#000033">
    <FORM ACTION="$ScriptURL?pid=$$" METHOD="POST">
    <input type="hidden" name="analyzelog" value="0">
    <TD ALIGN="CENTER" width="75%">
      $FONT<FONT COLOR="#FFFFFF"><B>Options:</B><BR>
      Lines:</font></font><INPUT TYPE="TEXT" NAME="numlines" VALUE="$FORM{numlines}" SIZE=3>~;
    print "&nbsp;" x 5, qq~$FONT<FONT COLOR="#FFFFFF">Order Returns:</font></font><SELECT NAME="reverse" SIZE=1>~;
    if($FORM{reverse} == 1) {
	print qq~<OPTION VALUE="1" SELECTED>Newest first</OPTION><OPTION VALUE="2">Oldest First</OPTION>~;
    } else {
	print qq~<OPTION VALUE="1">Newest first</OPTION><OPTION VALUE="2" SELECTED>Oldest First</OPTION>~;
    }
    print "</SELECT>", "&nbsp;" x 5, qq~<INPUT TYPE="SUBMIT" VALUE="Get"></FONT></FONT>
    </TD>
    </form>
    <FORM ACTION="$ScriptURL?pid=$$" METHOD="POST">
    <input type="hidden" name="analyzelog" value="1">
    <TD ALIGN="CENTER" width="25%">
      <input type="submit" value="Analyze">
    </td>
    </form>
  </TR>
</table><BR>$FONT
    ~;
}

sub PrintAccessLogFooter {
    print qq~
</font><BR>
<TABLE BORDER=2 WIDTH=100% CELLPADDING=5>
  <TR BGCOLOR="#000033">
    <TD ALIGN="CENTER">
      $FONT<FONT COLOR="#FFFFFF">
      &copy; 2000-2001 <A HREF="http://www.perlservices.net/"><FONT COLOR="#FFFFFF">Perl Services</FONT></A>
      </FONT></FONT>
    </TD>
  </TR>
</TABLE><BR><BR></BODY></HTML>
    ~;
}

sub split_entry {
    my $se_line = shift;
    my($se_ip, $se_datetime_offset, $se_datetime, $se_offset, $se_date, $se_time, $se_method, $se_document_compliancy, 
$se_document_qs, $se_compliancy, $se_document, $se_qs, $se_code, $se_size, $se_referer, $se_agent);
    my @se_datetime_vals;
    if($se_line =~ /^(.+) \- \- \[(.+)\] \"([a-zA-Z]+) (.+)\" ([0-9\-]+) ([0-9\-]+) \"(.+)\" \"(.+)\".*/) {   #"
       $se_ip       = $1;
       $se_datetime_offset = $2;
         ($se_datetime, $se_offset) = split(/ +/, $se_datetime_offset);
         my(@se_datetime_vals) = split(/\:/, $se_datetime);
         $se_date = $se_datetime_vals[0];
         $se_time = $se_datetime_vals[1] . ':' . $se_datetime_vals[2] . ':' . $se_datetime_vals[3];
        $se_method   = $3;
        $se_document_compliancy = $4;
          ($se_document_qs, $se_compliancy) = split(/ +/, $se_document_compliancy);
          ($se_document, $se_qs) = split(/\?/, $se_document_qs);
        $se_code     = $5;
        $se_size     = $6;
        $se_referer  = $7;
        $se_agent    = $8;
        return($se_ip, $se_date, $se_time, $se_method, $se_document_qs, $se_document, $se_qs, $se_code, $se_size, $se_referer, 
$se_agent);
    }
    elsif($se_line =~ /^(.+) \- \- \[(.+)\] \"([a-zA-Z]+) (.+)\" ([0-9\-]+) ([0-9\-]+).+/) {   #"
        $se_ip       = $1;
        $se_datetime_offset = $2;
          ($se_datetime, $se_offset) = split(/ +/, $se_datetime_offset);
          my(@se_datetime_vals) = split(/\:/, $se_datetime);
          $se_date = $se_datetime_vals[0];
          $se_time = $se_datetime_vals[1] . ':' . $se_datetime_vals[2] . ':' . $se_datetime_vals[3];
        $se_method   = $3;
        $se_document_compliancy = $4;
          ($se_document_qs, $se_compliancy) = split(/ +/, $se_document_compliancy);
          ($se_document, $se_qs) = split(/\?/, $se_document_qs);
        $se_code     = $5;
        $se_size     = $6;
        return($se_ip, $se_date, $se_time, $se_method, $se_document_qs, $se_document, $se_qs, $se_code, $se_size);
    } else {
        return(0);
    }
}


