#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/local/bin/perl

# ---------------------------------------------
# filename:	kProject.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	12.04.2003
# version:	v01_20041119
# ---------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;


# ------------------
# declarations
# ------------------

my @projects_file = &kCom_getIniValue("kProject","PATH_PROJECSTFILE");
my @projects_dir = &kCom_getIniValue("kProject","PATH_PROJECTS");

my $query_string = "$ENV{QUERY_STRING}";
my $type;

# ------------------
# main
# ------------------
&kCom_printTop("kProject");
if ($query_string eq "" ||$query_string =~ m/type=/){
	$type = $query_string;
	$type =~ s/type=//;
	&kProject_projectsOverview($type);
} elsif ($query_string =~ m/^show:(.*)/) {
	&kProject_showProjects($1);
}
&kCom_printBottom;

# ------------------
# sub
# ------------------


sub kProject_projectsOverview{

	my $viewType			= $_[0];

	my @name 				= &kCom_getIniValue($projects_file[0],"NAME",1);
	my @type 				= &kCom_getIniValue($projects_file[0],"TYPE",1);
	my @description			= &kCom_getIniValue($projects_file[0],"DESCRIPTION",1);
	my @status				= &kCom_getIniValue($projects_file[0],"STATUS",1);
	my @when				= &kCom_getIniValue($projects_file[0],"WHEN",1);
	my @link				= &kCom_getIniValue($projects_file[0],"LINK",1);
	my @html_titlecolor		= &kCom_getIniValue("kCom","HTML_TITLECOLOR");
	my @html_unevencolor	= &kCom_getIniValue("kCom","HTML_UNEVENCOLOR");
	my @dropdownlist 		= &kCom_getIniValue("kProject","DROPDOWNLIST");

	my $i;
	my $linecolour;
	my $projectNo			= 0;

	kCom_tableStart("Projects","","-1","2");
	print qq(
		<table width="100%" border="0">
		<tr>
			<td width="2%">&nbsp;</td>
			<td valign="top" width="20%"><font color=\"#$html_titlecolor[0]\"><b>Name:</b></font></td>
			<td valign="top" width="10%"><font color=\"#$html_titlecolor[0]\"><b>Type:</b></font></td>
			<td valign="top"><font color=\"#$html_titlecolor[0]\"><b>Description:</b></font></td>
			<td valign="top" width="10%"><font color=\"#$html_titlecolor[0]\"><b>Status:</b></font></td>
			<td valign="top" width="17%"><font color=\"#$html_titlecolor[0]\"><b>When:</b></font></td>
		</tr>
	);

	print qq(
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td valign="top" width="10%" height="30">
	);
	kCom_printDropdown("type", "kProject.cgi", "$viewType", @dropdownlist);
	print qq(
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	);

	for ($i=0; $i<$#link+1; $i++){

		if ($type[$i] eq $viewType || $viewType eq "." || $viewType eq ""){
			$projectNo++;

			if ($linecolour == 1){
				print "<tr bgcolor=\"$html_unevencolor[0]\">";
				$linecolour = 0;
			} else {
				print "<tr>";
				$linecolour = 1;
			}
	
			print qq (
					<td valign="top" align="right">$projectNo</td>
					<td valign="top" height="15"><b><a href="kProject.cgi?show:$link[$i]">$name[$i]</a></b></td>
					<td valign="top">$type[$i]</td>
					<td valign="top">$description[$i]</td>
					<td valign="top">$status[$i]</td>
					<td valign="top">$when[$i]</td>
				</tr>
			);
		}
	}

	print qq(</table>);
	kCom_tableStop;

}


sub kProject_showProjects{
	my $project	= $_[0];
	my $line;
	my $base;
	my $title;

	if ($project =~ m/\.\./){
		kCom_error("kProject_showProjects","Illegal project path: '$project'");
	}

	if ($project =~ m/\/(.*)\.\w+$/) {
		$title = $1;
	}

	kCom_tableStart("$title","","-1","2");

	$base = "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}"; 
	$base =~ s/cgi\/kProject\.cgi/$projects_dir[0]\/$project/;
	$base =~ s/\.\.\///;

    open(FILE, "<$projects_dir[0]/$project") || &kCom_error("kProject_showProjects","failed to open '$projects_dir[0]/$project'");
	print "<base href= \"$base\">\n";
	while ($line = <FILE>){
		print $line;
	}
	close(FILE);

	kCom_tableStop;

	$base = "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}"; 
	print "<base href= \"$base\">\n";


}

