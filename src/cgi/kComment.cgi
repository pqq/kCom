#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/local/bin/perl

# ---------------------------------------------
# filename:	kComment.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	05.03.2003
# version:	v01_20040430
# ---------------------------------------------

# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;

# ------------------
# declarations
# ------------------
my %FORM			= &kCom_parse;
my $comment_file	= $FORM{'comment_file'};
my $return_url		= $FORM{'return_url'};
my $email			= $FORM{'email'};
my $comment			= $FORM{'comment'};
my $vcode			= $FORM{'vcode'};


# ------------------
# main
# ------------------
if ($comment ne undef){
	if (&kCom_verifyCode($vcode) == 1){
		&kComment_registerComment;
	}
}
&kCom_jumpTo($return_url,1);


# ------------------
# sub
# ------------------
sub kComment_registerComment{
	my $timestamp = &kCom_timestamp();
	my $host_ip = &kCom_getHostIP;

	open (FILE, "<$comment_file");
	flock (FILE, 1);																				# share reading, don't allow writing
	my $content = join("",(<FILE>));
	close (FILE);
	flock (FILE, 8);																				# unlock file

	if ($content ne "" && $content !~ m/\n$/sg){													# have to do this check since new comments in the first place were written on top, but now on the bottom
		$content .= "\n";
	}

	open (FILE, ">$comment_file") || &kCom_error("kComment_registerComment","failed to open '$comment_file'");
	flock (FILE, 2);																				# lock file for writing	
	print FILE $content . "$timestamp���$host_ip���$email���$comment\n";
	close (FILE);
	flock (FILE, 8);																				# unlock file

}

