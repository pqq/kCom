#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/bin/perl

# ---------------------------------------------
# filename:	printHtml.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	10.12.2004
# version:	v01_20041210
# ---------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
my $base;
if ($^O eq "linux"){
	$base = '/home/klevstul/www/kCom/cgi/';
	use lib '/home/klevstul/www/kCom/cgi/';
	chdir("/home/klevstul/www/kCom/cgi/");
} else {
	$base = 'C:/klevstul/private/projects/kCom/cgi/';
	use lib 'C:/klevstul/private/projects/kCom/cgi/';
	chdir("C:/klevstul/private/projects/kCom/cgi/");	# have to change directory to get ini files right
}
use kCom;


# ------------------
# declarations
# ------------------
my %FORM = &kCom_parse;
my $urlbase = "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}"; 
my $type;
$urlbase =~ s/extra\/.*\.cgi.*//;


# ------------------
# main
# ------------------
&getParameters;
if ($type eq "top") {
	&kCom_printTop("", $urlbase);
} elsif ($type eq "bottom"){
	&kCom_printBottom;
} else {
	print "Content-type: text/html\n\n"; 
	&kCom_printError("printHtml.cgi", "Unknown parameters, please use 'type=[top|bottom]");
}





# ------------------
# sub
# ------------------
sub getParameters{

	$type = $FORM{'type'} || "top";

}

