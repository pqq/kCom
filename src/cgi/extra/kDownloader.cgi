#!/usr/bin/perl

# ------------------------------------------------------------------------------------
# filename:	kDownloader.cgi
# author:	Frode Klevstul (frode@klevstul.com) (www.klevstul.com)
# started:	02.04.2004
# ------------------------------------------------------------------------------------
#
# Revision - Newest version at the top - - - - - - - - - - - - - - - - - - - - - - - -
#
# version:  v01_20040402
#           - The first version of this program was made.
# ------------------------------------------------------------------------------------


# -------------------
# use
# -------------------
use strict;
use LWP::Simple;

# -------------------
# declarations
# -------------------
my $debug = 1;
my %FORM = &parse;
my $url = $FORM{'url'};

# -------------------
# main
# -------------------



# -------------------
# sub
# -------------------
&downloadObject($url);


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# downloads the object referred by the url
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub downloadObject{
	my $url	= $_[0];
	
	print "Content-type: text/html\n\n"; 

	if ($url !~ m/^http:\/\//){
		$url = "http://" . $url;
	}
	$url =~ s/"$//;																# sometimes (because of my substituteLinks regExp) there is a '"' at the end of the link we have to remove

	&debug($url);

	if ($url ne ""){
		my $object = &get($url);												# download the object
		$object = &substituteLinks($object);									# substitute links
		print $object;
	} else {
		&error("No url parameter specified");
	}

	&debug("EOF");
}

# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# downloads the object referred by the url
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub substituteLinks{
	my $object = $_[0];

	$object =~ s/a\s+href\s?=\s?\"?/a href=kDownloader.cgi?url=/sgi;
	
	return $object;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# prints out a debug message if debug is activated
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub debug{
	my $txt	= $_[0];
	if ($debug == 1){
		$txt .= "\n<br>";
		&printIt("kDownloader: $txt");
	}
}

# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# prints out an error message and dies / exits
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub error{
	my $txt	= $_[0];
	$txt .= "\n<br>";
	&printIt("ERROR: $txt");
	die("ERROR: $txt");
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# prints out an error message and dies / exits
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub printIt{
	my $txt	= $_[0];
	print "<font size=\"1\" color=\"orange\">$txt</font>";
}


# - - - - - - - - - - - -
# parse
# - - - - - - - - - - - -
sub parse {
    my ($name, $value, $buffer, $pair, $hold, @pairs);
    my %FORM;

    if($ENV{'REQUEST_METHOD'} eq 'GET') {
        @pairs = split(/&/, $ENV{'QUERY_STRING'});
    } else {
        read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
        @pairs = split(/&/, $buffer);
    }
    foreach $pair (@pairs) {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
	$name =~ s/\n//g;
	$name =~ s/\r//g;
	$value =~ s/\n\n/\n/g;			#Get's two newlines at linux all the time
	$value =~ s/\n/���/g;			#Modified to show linebreak
	$value =~ s/\r//g;				#Modified to show linebreak

	# converts into HTML special characters
	$value =~ s/</&lt;/g;
	$value =~ s/>/&gt;/g;
	$value =~ s/\|/&brvbar;/g;
	$value =~ s/grep/&#103;rep/g;
	$value =~ s/system/&#115;ystem/g;
	$value =~ s/\\/&#92;/g;
	$value =~ s/rm\s/ &#114;m /g;
	$value =~ s/rf\s/ &#114;f /g;

    #This section checks the value portion (user input) of
    #all name/value pairs.
	$value =~ s/(<[^>]*>)//g;		#Remove this tag to permit HTML tags
	$value =~ s/\|//g;
	$value =~ s/<!--(.|\n)*-->//g;
#	$value =~ s/\s-\w.+//g;
	$value =~ s/\0//g;
	$value =~ s/\|//g;
	$value =~ s/\\//g;
	$value =~ s/system\(.+//g;
	$value =~ s/grep//g;
	$value =~ s/\srm\s//g;
	$value =~ s/\srf\s//g;
#	$value =~ s/\.\.([\/\:]|$)//g;
	$value =~ s/< *((SCRIPT)|(APPLET)|(EMBED))[^>]+>//ig;
	$value =~ s/���/<br>/g;			#"linebreak" substituted to <br>

    #This section checks the value portion (from element name) of
    #all name/value pairs. This was included to prevent any nasty
    #surprises from those who would hijack you forms!
    $name =~ s/(<[^>]*>)//g;
	$name =~ s/<!--(.|\n)*-->//g;
	$name =~ s/\s-\w.+//g;
	$name =~ s/\0//g;
	$name =~ s/\|//g;
	$name =~ s/\\//g;
	$name =~ s/system\(.+//g;
	$name =~ s/grep//g;
	$name =~ s/\srm\s//g;
	$name =~ s/\srf\s//g;
	$name =~ s/\.\.([\/\:]|$)//g;
	$name =~ s/< *((SCRIPT)|(APPLET)|(EMBED))[^>]+>//ig;

	$FORM{$name} = $value;
    }

    return %FORM;
}
