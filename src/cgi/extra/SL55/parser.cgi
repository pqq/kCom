#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/bin/perl

# ---------------------------------------------
# filename:	parser.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	27.01.2004
# version:	v01_20040127
# ---------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
my $base;
if ($^O eq "linux"){
	$base = '/home/klevstul/www/kCom/cgi/';
	use lib '/home/klevstul/www/kCom/cgi/';
	chdir("/home/klevstul/www/kCom/cgi/");
} else {
	$base = 'C:/klevstul/private/projects/kCom/cgi/';
	use lib 'C:/klevstul/private/projects/kCom/cgi/';
	chdir("C:/klevstul/private/projects/kCom/cgi/");	# have to change directory to get ini files right
}
use kCom;


# ------------------
# declarations
# ------------------
my %FORM = &kCom_parse;
my $contact_file;
my $urlbase = "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}"; 
$urlbase =~ s/extra\/.*\.cgi.*//;


# ------------------
# main
# ------------------
&kCom_printTop("", $urlbase);
&getInputfile;
&parseData;
&kCom_printBottom;





# ------------------
# sub
# ------------------
sub getInputfile{

	# 2DO: read argument from user
	$contact_file = $FORM{'file'} || "Contacts.txt";
	$contact_file = $base . 'extra/SL55/' . $contact_file;

}


sub parseData{

	my $line;
	my $output;
	my $i=0;
	print qq(
		<table border="1">
	);

	#open contact file, and read data
	open (FILE , "<$contact_file") || &kCom_error("parser.cgi","failed to open '$contact_file'");

	while ($line = <FILE>){
		
		# Format:
		# 1     2      3     4     5    6    7     8     9        10 11  12      13    14      15      16     17   18      19
		# lname phhome ?(-1) group bday ?(0) fname phmob phoffice  ?  ?  company email email2  street  pcode  city country url

		my ($lname,$phhome,$f3,$group,$bday,$f6,$fname,$phmob,$phoffice,$f10,$f11,$company,$email,$email2,$street,$pcode,$city,$country,$url) = split('\t',$line);

		if ($i==1){
			$output = "<tr bgcolor=\"#99FF33\"><td valign=\"top\">";
			$i=0;
		} else {
			$output = "<tr><td valign=\"top\">";
			$i=1;
		}

		# firstname / lastname
		if ($fname ne "" || $lname ne ""){
			$output .= "" . $fname . " " . $lname . "";
		}

		# phonenumbers
		$output .= "&nbsp;</td><td valign=\"top\">";		
		if ($phmob ne ""){
			$output .= "m:" . $phmob . " ";
		}
		if ($phhome ne ""){
			$output .= "h:" . $phhome . " ";
		}
		if ($phoffice ne ""){
			$output .= "<br>o:" . $phoffice . " ";
		}

		# emails
		$output .= "&nbsp;</td><td valign=\"top\">";
		if ($email ne ""){
			$output .= "" . $email . " ";
		}
		if ($email2 ne ""){
			$email2 =~ s/\s//;
			$output .= "" . $email2 . " ";
		}
		
		# address
		$output .= "&nbsp;</td><td valign=\"top\">";
		if ($street ne "" || $pcode ne "" || $city ne "" || $country ne ""){
			$output .= "" . $street . ", " . $pcode . ", " . $city . ", " . $country;
		}

		# birthday
		$output .= "&nbsp;</td><td valign=\"top\">";
		if ($bday ne ""){
			my $year;
			my $month;
			my $date;

			$bday =~ s/T.*Z//;

			$bday =~ m/(\d{4})(\d{2})(\d{2})/;
			$year = $1;
			$month = $2;
			$date = $3;
			$output .= $date . "/" . $month . " " . $year . " ";
		}

		$output .= "&nbsp;</td></tr>\n";
		
		print qq($output);
	}
	close (FILE);

	print qq(
		</table>
	);


}


