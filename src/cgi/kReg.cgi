#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/local/bin/perl

# ---------------------------------------------
# filename:	kReg.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	14.08.2003
# version:	v01_20041119
# ---------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;


# ------------------
# declarations
# ------------------

my @registrations_file	= &kCom_getIniValue("kReg","PATH_REGISTRATIONSFILE");
my @registrations 		= &kCom_getIniValue("kReg","PATH_REGISTRATIONS");
my $query_string 		= "$ENV{QUERY_STRING}";


# ------------------
# main
# ------------------
&kCom_printTop("kReg");
if ($query_string eq ""){
	&kReg_regOverview;
} elsif ($query_string =~ m/^show:(.*)/) {
	&kReg_openReg($1);
} elsif ($query_string =~ m/^submit:/) {
	&kReg_addReg;
}
&kCom_printBottom;

# ------------------
# sub
# ------------------


sub kReg_regOverview{

	my @name 				= &kCom_getIniValue($registrations_file[0],"NAME",1);
	my @when				= &kCom_getIniValue($registrations_file[0],"WHEN",1);
	my @where				= &kCom_getIniValue($registrations_file[0],"WHERE",1);
	my @description			= &kCom_getIniValue($registrations_file[0],"DESCRIPTION",1);
	my @link				= &kCom_getIniValue($registrations_file[0],"LINK",1);
	my @status				= &kCom_getIniValue($registrations_file[0],"STATUS",1);
	my @html_titlecolor		= &kCom_getIniValue("kCom","HTML_TITLECOLOR");
	my @html_unevencolor 	= &kCom_getIniValue("kCom","HTML_UNEVENCOLOR");
	my $i;
	my $linecolour;

	kCom_tableStart("Registrations","","-1","2");
	print qq(
		<table width="100%" border="0">
		<tr>
			<td valign="top" width="12%"><font color=\"#$html_titlecolor[0]\"><b>Name:</b></font></td>
			<td valign="top" width="15%"><font color=\"#$html_titlecolor[0]\"><b>When:</b></font></td>
			<td valign="top" width="13%"><font color=\"#$html_titlecolor[0]\"><b>Where:</b></font></td>
			<td valign="top"><font color=\"#$html_titlecolor[0]\"><b>Description:</b></font></td>
		</tr>
	);

	for ($i=0; $i<$#link+1; $i++){
		if ( $status[$i]==0 ){
			next;
		}

		if ($linecolour == 1){
			print "<tr bgcolor=\"$html_unevencolor[0]\">";
			$linecolour = 0;
		} else {
			print "<tr>";
			$linecolour = 1;
		}

		print qq (
				<td valign="top"><b><a href="kReg.cgi?show:$link[$i]">$name[$i]</a></b></td>
				<td valign="top">$when[$i]</td>
				<td valign="top">$where[$i]</td>
				<td valign="top">$description[$i]</td>
			</tr>
		);
	}

	print qq(</table>);
	kCom_tableStop;
}


sub kReg_openReg{
	my $registration		= $_[0];
	my @titlecolor 			= &kCom_getIniValue("kCom","HTML_TITLECOLOR");
	my @html_unevencolor 	= &kCom_getIniValue("kCom","HTML_UNEVENCOLOR");
	my @display_hidden 		= &kCom_getIniValue("kCom","DISPLAY_HIDDEN");
	my @link 				= &kCom_getIniValue($registrations_file[0],"LINK",1);
	my @status				= &kCom_getIniValue($registrations_file[0],"STATUS",1);
	my @extra_info_desc 	= &kCom_getIniValue($registrations_file[0],"EXTRA_INFO_DESC",1);
	my $showdetails			= 0;
	my $showmailinglist		= 0;
	my $i;
	my $y;
	my $linecolour;
	my $verification_code	= &kCom_generateVerificationCode;

	if ($registration =~ m/^(.*)\?$display_hidden[0]$/){
		$registration = $1;
		$showdetails = 1;
		$showmailinglist = 1;
	} elsif ($registration =~ m/^(.*)\?.*$/){
		$registration = $1;
		$showdetails = 0;
	}

	my @name 				= &kCom_getIniValue($registrations[0]."/".$registration.".txt","NAME",1);
	my @email 				= &kCom_getIniValue($registrations[0]."/".$registration.".txt","EMAIL",1);
	my @extra 				= &kCom_getIniValue($registrations[0]."/".$registration.".txt","EXTRA",1);
	my @date 				= &kCom_getIniValue($registrations[0]."/".$registration.".txt","DATE",1);
	my @host 				= &kCom_getIniValue($registrations[0]."/".$registration.".txt","HOST_IP",1);

	LOOP: foreach (@link){
		if ($registration eq $_){
			last LOOP;
		}
		$i++;
	}

	my $this_status 		 = $status[$i];
	my $this_extra_info_desc = $extra_info_desc[$i];

	if (! -e $registrations[0]."/".$registration.".txt"){
		open (FILE, ">>$registrations[0]/$registration\.txt") || &kCom_error("kReg_addReg","failed to create new file '$registrations[0]/$registration\.txt'");
		flock (FILE, 2);		# lock file for writing
		close (FILE);
		flock (FILE, 8);		# unlock file
	}

	kCom_tableStart("$registration","","-1","2");

	# sign up form
	print qq(
		<form method="POST" action="kReg.cgi?submit:" name="kReg">
		<input type="hidden" name="registration" value="$registration">
		<table width="100%" border="0">
		<tr>
			<td width="15%">Name:</td>
			<td><input type="text" size="40" maxlength="50" name="name" value=""></td>
			<td width="300">&nbsp;</td>
		</tr>
		<tr>
			<td>Email:</td>
			<td><input type="text" size="40" maxlength="50" name="email"><font color="#$titlecolor[0]"></font></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Extra info:</td><td><input type="text" size="50" maxlength="300" name="extra"></td>
			<td>&nbsp;($this_extra_info_desc)</td>
		</tr>
		<tr><td colspan="3">For verification purposes please re-type the number <img src="kImgFromText.cgi?string=$verification_code&isCode=1">: <input type="text" name="vcode" size="4" maxlength="4"></td></tr>
		<tr><td colspan="3">&nbsp;</td></tr>
		<tr><td colspan="3">
		);
		
		if ($this_status == 1){
			print qq(<i>Note: Registered info will be hidden from publicity!</i>);
		} elsif ($this_status == 2){
			print qq(<i>Note: Email will be hidden!</i>);
		} elsif ($this_status == 3){
			print qq(<i>Note: Registered info will be viewable!</i>);
		}

		print qq(
			</td>
		</tr>
		<tr><td colspan="3" align="center"><a href="javascript:document.kReg.submit()">YES! Sign me up</a></td><td>&nbsp;</td></tr>
		<tr><td colspan="3">&nbsp;</td></tr>
		</table>
		</form>
	);

	# list email addresses
	if ($showmailinglist){
		print "<table width=\"100%\"><tr><td><b>Full email list:</b></td></tr><tr><td>";
		for ($i=0; $i<$#name+1; $i++){
			print $email[$i] . ", ";
		}
		print "</td></tr></table>";
	}


	# list out
	print qq(
		<table width="100%" border="0">
		<tr><td colspan="5">&nbsp;</td></tr>
		<tr><td colspan="5">&nbsp;</td></tr>
		<tr>
			<td width="3%">&nbsp;</td>
			<td width="17%"><font color="#$titlecolor[0]"><b>Name:</b></font></td>
			<td width="28%"><font color="#$titlecolor[0]"><b>Email:</b></font></td>
			<td><font color="#$titlecolor[0]"><b>Extra info:</b></font></td>
			<td width="27%"><font color="#$titlecolor[0]"><b>Timestamp & Host:</b></font></td>
		</tr>
	);

	for ($i=0; $i<$#name+1; $i++){
		$date[$i] = &kCom_printDate($date[$i],2);
		$y=$i+1;

		if ($linecolour == 1){
			print "<tr bgcolor=\"$html_unevencolor[0]\">";
			$linecolour = 0;
		} else {
			print "<tr>";
			$linecolour = 1;
		}

		print qq(
				<td valign="top" align="right">$y:</td>
		);
		if ($showdetails == 1){
			print qq(
				<td valign="top">$name[$i]</td>
				<td valign="top"><a href="mailto:$email[$i]">$email[$i]</a></td>
				<td valign="top">$extra[$i]</td>
				<td valign="top">$date[$i] \@ $host[$i]</td>
			);
		} else {
			if ($this_status == 1){
			print qq(
				<td valign="top"><font color="#$titlecolor[0]">Hidden</font></td>
				<td valign="top"><font color="#$titlecolor[0]">Hidden</font></td>
				<td valign="top"><font color="#$titlecolor[0]">Hidden</font></td>
				<td valign="top"><font color="#$titlecolor[0]">Hidden</font></td>
			);
			} elsif ($this_status == 2){
			print qq(
				<td valign="top">$name[$i]</td>
				<td valign="top"><font color="#$titlecolor[0]">Hidden</font></td>
				<td valign="top">$extra[$i]</td>
				<td valign="top">$date[$i] \@ $host[$i]</td>
			);
			} elsif ($this_status == 3){
			print qq(
				<td valign="top">$name[$i]</td>
				<td valign="top"><a href="mailto:$email[$i]">$email[$i]</a></td>
				<td valign="top">$extra[$i]</td>
				<td valign="top">$date[$i] \@ $host[$i]</td>
			);
			}
		}
		print qq(
			</tr>
		);
	}

	print qq(
		<tr><td colspan="5">&nbsp;</td></tr>
		</table>
	);


	kCom_tableStop;

}


sub kReg_addReg{
	my %FORM = &kCom_parse;
	my $registration = $FORM{'registration'};
	my $name = $FORM{'name'};
	my $email = $FORM{'email'};
	my $extra = $FORM{'extra'};
	my $vcode = $FORM{'vcode'};
	my $timestamp = &kCom_timestamp;
	my $host_ip = &kCom_getHostIP;

	if ($name ne "" && $email ne ""){
		if (&kCom_verifyCode($vcode) == 1){
			open (FILE, ">>$registrations[0]/$registration\.txt") || &kCom_error("kReg_addReg","failed to open '$registrations[0]/$registration\.txt'");
			flock (FILE, 2);		# lock file for writing
			print FILE "NAME\t$name\n";
			print FILE "EMAIL\t$email\n";
			print FILE "EXTRA\t$extra\n";
			print FILE "HOST_IP\t$host_ip\n";
			print FILE "DATE\t$timestamp\n";
			close (FILE);
			flock (FILE, 8);		# unlock file
		}
	}

	&kCom_jumpTo("kReg.cgi?show:$registration");
}
