#!/usr/local/bin/perl
#!C:/prgFiles/perl/bin/Perl.exe

# ---------------------------------------------
# filename: kBlogger.cgi
# author:   Frode Klevstul (frode@klevstul.com)
# started:  26.12.2006
$VERSION = 'v01_20061226';
# ---------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
use LWP::Simple;
if ($^O eq "linux"){
    use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;

# ------------------
# declarations
# ------------------
my @atom_feed       = &kCom_getIniValue("kBlogger","ATOM_FEED");
my @max_postings    = &kCom_getIniValue("kBlogger","MAX_POSTINGS");
my @blog_base_url   = &kCom_getIniValue("kBlogger","BLOG_BASE_URL");
my @path_comments   = &kCom_getIniValue("kBlogger","PATH_COMMENTS");
my $query_string    = $ENV{QUERY_STRING};


# ------------------
# main
# ------------------
&kCom_printTop("kBlogger");
if ($query_string =~ m/^show:(.*)/ || $query_string eq undef) {
    &kBlogger_getBlogContent($1);
} elsif ($query_string =~ m/^com:(.*)/) {
    &kBlogger_addComment($1);
}
&kCom_printBottom;


# ------------------
# sub
# ------------------
sub kBlogger_getBlogContent {
    my $url = $_[0];

    my $webObject;
    my @entries;
    my $title;
    my $published;
    my $content;
    my $post_link;
    my $author;
    my $counter;
    my $show_single;
    my $post_id = $url;
    $post_id =~ s/.*\/(.*$)/$1/;

    # if the URL is empty it meens we're going to list out many postings
    if ($url ne undef){
        $show_single = 1;
    } else {
        $show_single = 0;
    }

    # download the web object
    if ($show_single == 0){
        $webObject = &get($atom_feed[0]);
    } else {
        $webObject = &get($blog_base_url[0].$url);
    }

    # HTMLify
    $webObject =~ s/&gt;/>/sg;
    $webObject =~ s/&lt;/</sg;

    &kCom_tableStart("kBlogger","","-1","2");

    # check if the feed is proper Atom feed
    if ($webObject !~ m/xmlns='http:\/\/www.w3.org\/2005\/Atom'/sgi){
        print "<b>Warning: Not recognised Atom Feed!</b><br>";
    }

    # strip away everything before the first <entry> tag
    $webObject =~ s/^.*?(<entry>.*$)/$1/sgi;

    # strip away everything after the last </entry> tag
    $webObject =~ s/(^.*<\/entry>).*$/$1/sgi;

    # split the entries and push it onto an array
    @entries = split('</entry>', $webObject);

    # go through the entries
    FOREACH: foreach (@entries){
        $counter++;

        # ---
        # select out different XML formatted values
        # ---
        if ($_ =~ m/<published.*?>(.*)<\/published>/sgi){
            $published = $1;
            $published =~ s/(.*)T\d{2}:.*/$1/sgi;
        }

        if ($_ =~ m/<title.*?>(.*)<\/title>/sgi){
            $title = $1;
        }

        if ($_ =~ m/<content.*?>(.*)<\/content>/sgi){
            $content = $1;
            $content =~ s/&amp;/&/sg;
        }

        if ($_ =~ m/<link rel='self' type='application\/atom\+xml' href='(.*?)'><\/link>/sgi){
            $post_link = $1;
            $post_link =~ s/$blog_base_url[0]//s;
        }

        if ($_ =~ m/<author.*?>(.*)<\/author>/sgi){
            $author = $1;
            if ($author =~ m/<name.*?>(.*)<\/name>/sgi){
                $author = $1;
            }
        }

        # ---
        # print HTML
        # ---
        if ($show_single == 1) {
            print qq(
                <tr><td><b><a href="kBlogger.cgi"><font size="2">$title</font></a></b></td></tr>
                <tr><td>&nbsp;</td></tr>
            );
        } else {
            print qq(
            	<tr><td><b><a href="kBlogger.cgi?show:$post_link"><font size="2">$title</font></b></a></td></tr>
                <tr><td>&nbsp;</td></tr>
            );
        }

        print qq(
            <tr><td>$content</td></tr>
            <tr><td>[ $author | $published ]</td></tr>
            <tr><td><hr noshade size="1"></td></tr>
        );

        if ($show_single == 1) {
            print qq(
                <tr><td align="right">[<a href="kBlogger.cgi?com:$url">add comment</a>]</td></tr>
            );
            if (-e "$path_comments[0]/$post_id\.com"){
                print qq(<tr><td>);
                &kCom_listComments("$path_comments[0]/$post_id\.com");
                print qq(</td></tr>);
            }
        }

        # stop looping when we've found enough postings
        if ($counter == $max_postings[0]){
            last FOREACH;
        }
    }

    &kCom_tableStop;
}


sub kBlogger_addComment{
    my $url = $_[0];
    my $post_id = $url;
    $post_id =~ s/.*\/(.*$)/$1/;
    my $return_url = "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}?show:$url";

    $return_url =~ s/com:/show:/;
    &kCom_addComment("$path_comments[0]/$post_id\.com",undef,$return_url);
}
